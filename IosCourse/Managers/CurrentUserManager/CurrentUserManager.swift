//
//  CurrentUserManager.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 11.11.2020.
//

import Foundation
import FirebaseAuth

protocol CurrentUserManaging {
    var currentUser: Emitter<CurrentUserModel?>? { get }
    
    func setUser(currentUserModel: CurrentUserModel)
    func logOut(_ completion: (() -> Void)?)
}

class CurrentUserManager: CurrentUserManaging {
    
    func setUser(currentUserModel: CurrentUserModel) {
        if self.currentUser != nil {
            self.currentUser?.value = currentUserModel
        } else {
            self.currentUser = Emitter(currentUserModel)
        }

        self.isLoggedIn.value = true
        UserDefaults.standard.set(isLoggedIn.value, forKey: "isLoggedIn")
        CoreDataService.shared.addEntity(entity: .CurrentUserEntity(currentUserModel))

        guard let pilotModel = currentUserModel.pilotModel else {
            CoreDataService.shared.saveChanges()
            return
        }
        CoreDataService.shared.addEntity(entity: .PilotEntity(pilotModel))
        CoreDataService.shared.saveChanges()
        
    }
    
    
    var currentUser: Emitter<CurrentUserModel?>?
    var isLoggedIn: Emitter<Bool>
    private var currentUserEntity: CurrentUserEntity? {
        CoreDataService.shared.getCurrentUser()
    }
    
    static let shared = CurrentUserManager()
    
    private init() {
        self.isLoggedIn = Emitter<Bool>(UserDefaults.standard.bool(forKey: "isLoggedIn"))
        
        guard let currentUserEntity = self.currentUserEntity else {
            return
        }
        
        var user = CurrentUserModel(entity: currentUserEntity)
        if user.isPilot {
            guard let pilotEntity = CoreDataService.shared.getCurrentPilot() else {
                return
            }
            let pilotModel = PilotModel(entity: pilotEntity)
            user.pilotModel = pilotModel
        }
        self.currentUser = Emitter(user)
    }
    
    
    // TODO: completion
    func becomePilot(phoneNumber: String, copterName: String, profileImage: UIImage, completion: @escaping (Result<Any?, Error>) -> ()) {
        guard var currentUser = self.currentUser?.value else {
            return
        }
        StorageService.shared.saveProfileImage(uid: currentUser.uid, image: profileImage) { (result) in
            switch result {
            case .success(let path):
                let nameSurname = "\(currentUser.firstName) \(currentUser.lastName)"
                var pilotModel = PilotModel(uid: currentUser.uid, nameSurname: nameSurname, phoneNumber: phoneNumber, copterName: copterName, profileImagePath: path, worksIds: [])
                currentUser.isPilot = true
                pilotModel.profileImage = profileImage
                currentUser.pilotModel = pilotModel
                self.currentUser?.value = currentUser
                FirestoreService.shared.becomePilot(pilotModel: pilotModel) { (result) in
                    switch result {
                    case .success(_):
                        completion(.success(nil))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func logOut(_ completion: (() -> Void)?) {
        CoreDataService.shared.clearAllCoreData()
        self.isLoggedIn.value = false
        self.currentUser?.value = nil
        
        do{
            try Auth.auth().signOut();
          } catch let logoutError {
            print(logoutError)
          }
        
        UserDefaults.standard.set(isLoggedIn.value, forKey: "isLoggedIn")
        completion?()
    }
    
}
