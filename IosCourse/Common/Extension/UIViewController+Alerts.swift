//
//  UIViewController+Alerts.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 10.11.2020.
//

import UIKit

extension UIViewController {
    
    func showAlert(with title: String? = nil, message: String?, okHandler: ((UIAlertAction) -> ())? = nil, completion: (() -> ())? = nil) {
        guard let message = message, !message.isEmpty else {
            return
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: okHandler))
        
        present(alertController, animated: true, completion: completion)
    }
    
    func showErrorAlert(title: String = "Ошибка", message: String, okHandler: ((UIAlertAction)->Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: okHandler))

        present(alert, animated: true)
    }
    
    func showMessageAlert(title: String = "Сообщение", message: String, okHandler: ((UIAlertAction)->Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: okHandler))

        present(alert, animated: true)
    }
    
    func showConformationAlert(title: String = "Сообщение", message: String, actionHandler: ((UIAlertAction)->Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: actionHandler))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: actionHandler))

        present(alert, animated: true)
    }
}
