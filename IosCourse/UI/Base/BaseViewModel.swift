//
//  BaseViewModel.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 10.11.2020.
//
//  Base class for all ViewModel's in the app.

import Foundation

public protocol BaseViewModeling: class {
    var isLoading: Bool { get }
    
    var didChange: (() -> Void)? { get set }
    var didGetError: ((_ message: String) -> Void)? { get set }
}

public class BaseViewModel: NSObject, BaseViewModeling {
    
    public var isLoading = false {
        didSet {
            didChange?()
        }
    }
    
    public var didChange: (() -> Void)? {
        didSet {
            didChange?()
        }
    }
    
    public var didGetError: ((_ message: String) -> Void)?
}



