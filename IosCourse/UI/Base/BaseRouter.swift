//
//  BaseRouter.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 10.11.2020.
//
//  Navigate to the most common ViewControlles, like popups, alerts, and tosts

import UIKit

public protocol BaseRouting {
    
    var navigationController: UINavigationController { get }
}

public class BaseRouter {
    
    public let navigationController: UINavigationController
    
    init() {
        let nvc = UINavigationController()
        nvc.navigationBar.isHidden = true
        nvc.modalPresentationStyle = .fullScreen
        //disable pop swipe from left to right
        nvc.interactivePopGestureRecognizer?.isEnabled = false
        
        self.navigationController = nvc
    }
}

extension BaseRouter: BaseRouting {

}
