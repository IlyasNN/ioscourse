//
//  CurrentUserModel.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 11.11.2020.
//

import Foundation
import FirebaseFirestore

struct CurrentUserModel {
    
    var uid: String
    var firstName: String
    var lastName: String
    var email: String
    var isPilot: Bool
    var pilotModel: PilotModel?
    
    var dictionaryRepresentation: [String: Any] {
        return [
            "uid" : uid,
            "firstname": firstName,
            "lastname": lastName,
            "email": email,
            "isPilot": isPilot
        ]
    }
    
    init(uid: String,
         firstName: String,
         lastName: String,
         email: String) {
        
        self.uid = uid
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.isPilot = false
    }
    
    init(uid: String,
         firstName: String,
         lastName: String,
         email: String,
         isPilot: Bool) {
        
        self.uid = uid
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.isPilot = isPilot
    }
    
    init(entity: CurrentUserEntity) {
        self.init(uid: entity.uid,
                  firstName: entity.firstName,
                  lastName: entity.lastName,
                  email: entity.email,
                  isPilot: entity.isPilot)
    }
    
    init?(document: DocumentSnapshot) {
        guard let data = document.data() else {
            return nil
        }
        self.init(data: data)
    }
    
    private init?(data: [String: Any]) {
        guard let uid = data["uid"] as? String,
              let firstName = data["firstname"] as? String,
              let lastName = data["lastname"] as? String,
              let email = data["email"] as? String,
              let isPilot = data["isPilot"] as? Bool else {
            return nil
        }
        self.init(uid: uid, firstName: firstName, lastName: lastName, email: email, isPilot: isPilot)
    }
    
}

extension CurrentUserModel: Hashable {
    
}

extension CurrentUserModel: Equatable {
    
}
