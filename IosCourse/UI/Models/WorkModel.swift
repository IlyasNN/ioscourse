//
//  WorkModel.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 16.12.2020.
//

import UIKit

struct WorkModel {
    
    var placeModel: PlaceModel
    var pilotModel: PilotModel
    var image: UIImage
    
    init(placeModel: PlaceModel, pilotModel: PilotModel, image: UIImage) {
        self.placeModel = placeModel
        self.pilotModel = pilotModel
        self.image = image
    }
}

extension WorkModel: Hashable {
    
}
