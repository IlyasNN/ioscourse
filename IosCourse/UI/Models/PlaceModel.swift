//
//  PlaceModel.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 09.12.2020.
//

import Foundation
import FirebaseFirestore

struct PlaceModel {
    
    var id: String
    var ownerId: String
    var name: String
    var descriptionPlace : String
    var latitude : Double
    var longitude : Double
    var imagePaths: [String]
    
    var dictionaryRepresentation: [String: Any] {
        return [
            "id" : id,
            "ownerId": ownerId,
            "name": name,
            "descriptionPlace": descriptionPlace,
            "latitude": latitude,
            "longitude": longitude,
            "imagePaths": imagePaths
        ]
    }
    
    init(id: String,
         ownerId: String,
         name: String,
         descriptionPlace: String,
         latitude : Double,
         longitude : Double,
         imagePaths: [String]) {
        
        self.id = id
        self.ownerId = ownerId
        self.name = name
        self.descriptionPlace = descriptionPlace
        self.latitude = latitude
        self.longitude = longitude
        self.imagePaths = imagePaths
    }
    
    //    init(entity: PlaceEntity) {
    //        self.init(id: String,
    //                    name: String,
    //                    descriptionPlace: String,
    //                    latitude : Double,
    //                    longitude : Double)
    //    }
    
    init?(document: DocumentSnapshot) {
        guard let data = document.data() else {
            return nil
        }
        self.init(data: data)
    }
    
    private init?(data: [String: Any]) {
        guard let id = data["id"] as? String,
              let ownerId = data["ownerId"] as? String,
              let name = data["name"] as? String,
              let descriptionPlace = data["descriptionPlace"] as? String,
              let latitude = data["latitude"] as? Double,
              let longitude = data["longitude"] as? Double,
              let imagePaths = data["imagePaths"] as? [String] else {
            return nil
        }
        self.init(id: id,
                  ownerId: ownerId,
                  name: name,
                  descriptionPlace: descriptionPlace,
                  latitude: latitude,
                  longitude: longitude,
                  imagePaths: imagePaths)
    }
    
}

extension PlaceModel: Hashable {
    
}

extension PlaceModel: Equatable {
    
}
