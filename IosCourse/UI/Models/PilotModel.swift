//
//  PilotModel.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 13.12.2020.
//

import UIKit
import FirebaseFirestore

struct PilotModel {
    
    let nameSurname: String
    let uid: String
    let phoneNumber: String
    let copterName: String
    let profileImagePath: String
    var profileImage: UIImage?
    var worksIds: [String]
    
    var dictionaryRepresentation: [String: Any] {
        return [
            "uid" : uid,
            "nameSurname": nameSurname,
            "phoneNumber": phoneNumber,
            "copterName": copterName,
            "profileImagePath": profileImagePath,
            "worksIds": worksIds,
        ]
    }
    
    init(uid: String,
         nameSurname: String,
         phoneNumber: String,
         copterName: String,
         profileImagePath: String,
         worksIds: [String]) {
        self.nameSurname = nameSurname
        self.uid = uid
        self.phoneNumber = phoneNumber
        self.copterName = copterName
        self.profileImagePath = profileImagePath
        self.worksIds = worksIds
    }
    
    init(entity: PilotEntity) {
        self.init(uid: entity.uid, nameSurname: entity.nameSurname, phoneNumber: entity.phoneNumber, copterName: entity.copterName, profileImagePath: entity.profileImagePath, worksIds: entity.worksIds)
        guard let imageData = entity.profileImage else {
            return
        }
        self.profileImage = UIImage(data: imageData)
    }
    
    init?(document: DocumentSnapshot) {
        guard let data = document.data() else {
            return nil
        }
        self.init(data: data)
    }
    
    private init?(data: [String: Any]) {
        guard let uid = data["uid"] as? String,
              let nameSurname = data["nameSurname"] as? String,
              let phoneNumber = data["phoneNumber"] as? String,
              let copterName = data["copterName"] as? String,
              let profileImagePath = data["profileImagePath"] as? String,
              let worksIds = data["worksIds"] as? [String] else {
            return nil
        }
        self.init(uid: uid,
                  nameSurname: nameSurname,
                  phoneNumber: phoneNumber,
                  copterName: copterName,
                  profileImagePath: profileImagePath,
                  worksIds: worksIds)
    }
    
}

extension PilotModel: Hashable {
    
}

extension PilotModel: Equatable {
    
}
