//
//  MapFlowRouter.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 06.11.2020.
//

import UIKit

public protocol MapFlowRouting: class, BaseRouting {
    func presentMapViewController(_ completion: (()->Void)?)
    func dissmiss(_ completion: (() -> Void)?)
}

final class MapFlowRouter: BaseRouter {
    
    weak var mainTabBarRouter: MainTabBarRouting?
    
    init(with mainTabBarRouter: MainTabBarRouting) {
        self.mainTabBarRouter = mainTabBarRouter
        
        super.init()
    }
    
    func dissmiss(_ completion: (() -> Void)?) {
        let cleanVC = CleanViewController.initFromItsStoryboard()
        navigationController.setViewControllers([cleanVC], animated: false)
    }
}

extension MapFlowRouter: MapFlowRouting {
    
    func presentMapViewController(_ completion: (() -> Void)?) {
        if let presentedRVC = navigationController.viewControllers.filter({ $0 is MapViewController}).first {
            navigationController.popToViewController(presentedRVC, animated: true)
        } else {
            let vc = MapViewController.initFromItsStoryboard()
            vc.viewModel = MapViewModel()
            vc.router = self
            vc.modalPresentationStyle = .fullScreen
            
            navigationController.viewControllers.removeAll()
            if navigationController.viewControllers.count == 0 {
                navigationController.viewControllers.append(vc)
            } else {
                navigationController.pushViewController(vc, animated: true)
            }
        }
    }
    
}

extension MapFlowRouter: MapRouting {
    
    func presentPlaceViewController(placeModel: PlaceModel, _ completion: (() -> Void)?) {
        if let presentedRVC = navigationController.viewControllers.filter({ $0 is PlaceViewController}).first {
            navigationController.popToViewController(presentedRVC, animated: true)
        } else {
            let vc = PlaceViewController.initFromItsStoryboard()
            vc.viewModel = PlaceViewModel(placeModel: placeModel)
            vc.router = self
            vc.modalPresentationStyle = .fullScreen
            
            if navigationController.viewControllers.count == 0 {
                navigationController.viewControllers.append(vc)
            } else {
                navigationController.pushViewController(vc, animated: true)
            }
        }
        completion?()
    }
    
    func presentAddPlaceViewController(latitude: Double, longitude: Double, _ completion: (() -> Void)?) {
        if let presentedRVC = navigationController.viewControllers.filter({ $0 is AddPlaceViewController}).first {
            navigationController.popToViewController(presentedRVC, animated: true)
        } else {
            let vc = AddPlaceViewController.initFromItsStoryboard()
            vc.viewModel = AddPlaceViewModel(latitude: latitude, longitude: longitude)
            vc.router = self
            vc.modalPresentationStyle = .fullScreen
            
            if navigationController.viewControllers.count == 0 {
                navigationController.viewControllers.append(vc)
            } else {
                navigationController.pushViewController(vc, animated: true)
            }
        }
        completion?()
    }
}

extension MapFlowRouter: AddPlaceRouting {
    
}

extension MapFlowRouter: PlaceRouting {
    func navigateBack(_ completion: (() -> Void)?) {
        navigationController.popViewController(animated: true)
    }
}

