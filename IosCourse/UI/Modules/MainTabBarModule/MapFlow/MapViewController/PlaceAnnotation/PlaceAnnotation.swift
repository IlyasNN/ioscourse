//
//  PlaceAnnotation.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 09.12.2020.
//

import Foundation
import MapKit

final class PlaceAnnotation: MKPointAnnotation {
    let placeModel: PlaceModel
    
    init(placeModel: PlaceModel) {
        self.placeModel = placeModel
        super.init()
        self.coordinate = CLLocationCoordinate2D(latitude: placeModel.latitude, longitude: placeModel.longitude)
        self.title = placeModel.name
    }
}
