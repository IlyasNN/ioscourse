//
//  MapViewModel.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 06.11.2020.
//

import Foundation

class MapViewModel: BaseViewModel {
    
    var placeAnnotations: [PlaceAnnotation] = []
    
    override init() {
        super.init()
        
        FirestoreService.shared.getPlaces { [weak self] (result) in
            switch result {
            
            case .success(let placeModels):
                self?.placeAnnotations = placeModels.map({
                    PlaceAnnotation(placeModel: $0)
                })
                self?.didChange?()
            case .failure(let error):
                self?.didGetError?(error.localizedDescription)
            }
        }
    }
    
}

extension MapViewModel: MapViewModeling {
    
}
