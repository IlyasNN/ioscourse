//
//  MapViewController.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 06.11.2020.
//

import UIKit
import MapKit

protocol MapRouting: BaseRouting {
    func presentAddPlaceViewController(latitude: Double, longitude: Double, _ completion: (() -> Void)?)
    func presentPlaceViewController(placeModel: PlaceModel, _ completion: (() -> Void)?)
}

protocol MapViewModeling: BaseViewModeling {
    var placeAnnotations: [PlaceAnnotation] { get }
}

class MapViewController: UIViewController {
    
    private enum MapViewControllerMode {
        case defaultMode
        case editingMode
    }
    
    private var firstSetup = true
    
    private var mode: MapViewControllerMode = .defaultMode {
        didSet {
            updateMode(mode)
        }
    }
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var cancelButton: DesignableButton!
    @IBOutlet weak var addButton: DesignableButton!
    @IBOutlet weak var mapPinImage: UIImageView!
    
    var router: MapRouting?
    var viewModel: MapViewModeling? {
        didSet {
            viewModel?.didChange = { [weak self] in
                DispatchQueue.main.async { [weak self] in
                    self?.update()
                }
            }
            
            viewModel?.didGetError = { [weak self] message in
                DispatchQueue.main.async { [weak self] in
                    self?.showAlert(message: message)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        update()
    }
    
    private func setup() {
        mode = .defaultMode
        mapView.showsUserLocation = true
    }
    
    private func update() {
        guard let viewModel = viewModel, isViewLoaded else {
            return
        }
        for annotation in viewModel.placeAnnotations {
            mapView.addAnnotation(annotation)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        locationPermissionCheck()
        
        
        guard let location = LocationService.shared.currentLocation.value else {
            return
        }
        
        if firstSetup {
            firstSetup = false
            self.updateCurrentLocation(latitude: location.latitude, longitude: location.longitude)
        }
    }
    
    private func updateCurrentLocation(latitude: Double, longitude: Double) {
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), latitudinalMeters: 5000, longitudinalMeters: 5000)
        mapView.setRegion(region, animated: true)
    }
    
    private func updateMode(_ mode: MapViewControllerMode) {
        switch mode {
        case .defaultMode:
            cancelButton.isHidden = true
            mapPinImage.isHidden = true
            addButton.backgroundColor = UIColor.init(named: "black.background")
        case .editingMode:
            cancelButton.isHidden = false
            mapPinImage.isHidden = false
            addButton.backgroundColor = .red
        }
    }
    
    private func locationPermissionCheck() {
        let permission = LocationService.shared.permissionStatus
        
        if !permission {
            showLocationPermissionAlert()
        }
    }
    
    private func showLocationPermissionAlert() {
        let alert = UIAlertController(title: "У вас выключена служба геолокации", message: "Необходимы точные координаты вашего местоположения. Хотите включить?", preferredStyle: .alert)
        let settings = UIAlertAction(title: "Настройки", style: .default) { (alert) in
            if let url = URL(string: "App-prefs:root=LOCATION_SERVICES") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        alert.addAction(settings)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func locationButtonTapped(_ sender: Any) {
        guard let location = LocationService.shared.currentLocation.value else {
            return
        }
        self.updateCurrentLocation(latitude: location.latitude, longitude: location.longitude)
    }
    
    @IBAction func addButtonTapped(_ sender: Any) {
        switch mode {
        case .defaultMode:
            guard CurrentUserManager.shared.currentUser?.value?.isPilot == true else {
                showAlert(message: "Чтобы добавить точку на карту, станьте пилотом в разделе Профиль")
                return
            }
            mode = .editingMode
            break
        case .editingMode:
            let center = mapView.centerCoordinate
            router?.presentAddPlaceViewController(latitude: center.latitude, longitude: center.longitude) {
                self.mode = .defaultMode
            }
            break
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        mode = .defaultMode
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let placeAnnotation = view.annotation as? PlaceAnnotation else {
            return
        }
        
        let placeModel = placeAnnotation.placeModel
        router?.presentPlaceViewController(placeModel: placeModel) {
            mapView.deselectAnnotation(placeAnnotation, animated: true)
        }
        
    }
}
