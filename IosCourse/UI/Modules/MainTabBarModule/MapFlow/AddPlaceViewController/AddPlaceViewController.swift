//
//  AddPlaceViewController.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 30.11.2020.
//

import UIKit
import Photos

protocol AddPlaceRouting: BaseRouting {
    func presentMapViewController(_ completion: (() -> Void)?)
}

protocol AddPlaceViewModeling: BaseViewModeling {
    var name: String? { get set }
    var descriptionPlace: String? { get set }
    var isSavingAvailable: Bool { get }
    var descriptionTextViewPlaceHolder: String { get }
    var defaultTextViewHeight: Float { get }
    var photoImages: [PhotoImage] { get }
    var cellHorizontalSpace: Float { get }
    var imagesMaxCount: Int { get }
    func addImage(image: UIImage)
    func removeImage(at index: Int)
    
    func savePlace()
    
    var didSuccessfullySaved: (() -> Void)? { get set }
    
    var didGetError: ((String) -> Void)? { get set }
}

fileprivate enum Section {
    case main
}

fileprivate typealias DataSource = UICollectionViewDiffableDataSource<Section, PhotoImage>
fileprivate typealias DataSourceSnapshot = NSDiffableDataSourceSnapshot<Section, PhotoImage>


class AddPlaceViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameTextField: DesignableTextField!
    @IBOutlet weak var descriptionTextView: DesignableTextView!
    @IBOutlet weak var descriptionTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var saveButton: DesignableButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var router: AddPlaceRouting?
    var viewModel: AddPlaceViewModeling? {
        didSet {
            viewModel?.didChange = { [weak self] in
                DispatchQueue.main.async { [weak self] in
                    self?.update()
                }
            }
            
            viewModel?.didSuccessfullySaved = { [weak self] in
                DispatchQueue.main.async {
                    self?.router?.presentMapViewController(nil)
                }
            }
            
            viewModel?.didGetError = { [weak self] message in
                DispatchQueue.main.async { [weak self] in
                    self?.updateErrorLabel(message)
                }
            }
        }
    }
    
    private lazy var dataSource = setupDataSource()
    private let imagePickerService = ImagePickerService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        setup()
        updateDataSource()
    }
    
    @objc func keyboardWillShow(notification:NSNotification) {

        guard let userInfo = notification.userInfo else { return }
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)

        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 20
        scrollView.contentInset = contentInset
    }

    @objc func keyboardWillHide(notification:NSNotification) {

        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    private func setup() {
        descriptionTextView.text = viewModel?.descriptionTextViewPlaceHolder
        
        // CollectionView
        photosCollectionView.dataSource = dataSource
        let reuseId = PhotoCollectionViewCell.typeName
        photosCollectionView.register(UINib(nibName: reuseId, bundle: nil),
                                forCellWithReuseIdentifier: reuseId)
        
        // ImagePickerService
        imagePickerService.delegate = self
        imagePickerService.presentingViewController = self
        
        // Other
        errorLabel.alpha = 0
        saveButton.isEnabled = false
        loadingIndicator.isHidden = true
    }
    
    private func setupDataSource() -> DataSource {
        let dataSource = DataSource(
            collectionView: photosCollectionView,
            cellProvider: { (collectionView, indexPath, photoImage) ->
                UICollectionViewCell? in
                // 2
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: PhotoCollectionViewCell.typeName,
                    for: indexPath) as? PhotoCollectionViewCell
                cell?.imageView.image = photoImage.image
                return cell
            })
        return dataSource
    }
    
    private func update() {
        guard isViewLoaded else {
            return
        }
        updateDataSource()
        updateSaveButton()
        updateLoadingState()
    }
    
    private func updateErrorLabel(_ message: String) {
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    private func resetErrorLabel() {
        errorLabel.alpha = 0
    }
    
    private func updateSaveButton() {
        saveButton.isEnabled = viewModel?.isSavingAvailable ?? false
    }
    
    private func updateLoadingState() {
        let isLoading = viewModel?.isLoading ?? false
        saveButton.isHidden = isLoading
        loadingIndicator.isHidden = isLoading == false
        isLoading ? loadingIndicator.startAnimating() : loadingIndicator.stopAnimating()
    }
    
    private func updateDataSource(animatingDifferences: Bool = true) {
        guard let viewModel = viewModel else {
            return
        }
        var snapshot = DataSourceSnapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(viewModel.photoImages, toSection: .main)
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    func showSourceSheet() {
        let alertVC = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertVC.addAction(UIAlertAction(title: "Сделать фото",
                                        style: .default,
                                        handler: { [weak self] _ in
            self?.imagePickerService.pickPhotoFromCamera()
        }))
        
        alertVC.addAction(UIAlertAction(title: "Выбрать из галереи",
                                        style: .default,
                                        handler: { [weak self] _ in
            self?.imagePickerService.pickPhotoFromLibrary()
        }))
        
        alertVC.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        present(alertVC, animated: true, completion: nil)
    }
    
    private func presentImagePreview(index: Int) {
        let image = viewModel?.photoImages[index].image
        
        let imagePreview: ImagePreviewView = .fromNib()
        
        imagePreview.setup(image: image)
        
        imagePreview.deleteHandler = { [weak self] in
            self?.viewModel?.removeImage(at: index)
            imagePreview.removeFromSuperview()
        }
        
        view.addSubview(imagePreview)
        imagePreview.frame = view.frame
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        router?.presentMapViewController(nil)
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        resetErrorLabel()
        viewModel?.savePlace()
    }
    
    @IBAction func textFieldEditingChanged(_ sender: Any) {
        viewModel?.name = nameTextField.text
        resetErrorLabel()
        update()
    }
    
    @IBAction func textFieldReturnButtonTapped(_ sender: Any) {
        view.endEditing(true)
    }
}

extension AddPlaceViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        viewModel?.descriptionPlace = textView.text
        resetErrorLabel()
        update()
        
        let numLines = Int(textView.contentSize.height / textView.font!.lineHeight)
        
        guard let viewModel = viewModel else {
            return
        }
        guard numLines > 2 else {
            descriptionTextViewHeight.constant = CGFloat(viewModel.defaultTextViewHeight)
            UIView.animate(withDuration: 0.1,
                           delay: 0,
                           options: .curveEaseInOut,
                           animations: {
                            self.descriptionTextView.layoutIfNeeded()
                           },
                           completion: nil)
            return
        }
        
        guard numLines < 5 else {
            return
        }
        
        let newTextViewHeight = textView.contentSize.height
        guard newTextViewHeight > CGFloat(viewModel.defaultTextViewHeight),
              descriptionTextViewHeight.constant != newTextViewHeight else {
            return
        }
        
        descriptionTextViewHeight.constant = newTextViewHeight
        UIView.animate(withDuration: 0.1,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {
                        self.descriptionTextView.layoutIfNeeded()
                       },
                       completion: nil)
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == viewModel?.descriptionTextViewPlaceHolder {
            textView.textColor = .black
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = .placeholderText
            textView.text = viewModel?.descriptionTextViewPlaceHolder
            //setSendButtonTappedEnabled(false)
        } else {
            //setSendButtonTappedEnabled(true)
        }
    }
}

extension AddPlaceViewController: ImagePickerServiceDelegate {
    
    func imagePickerService(finishWith image: UIImage) {
        viewModel?.addImage(image: image)
    }
    
    func imagePickerService(finishWith error: ImagePickerError) {
        
        let title: String
        let message: String
        
        switch error {
        case .noRightsForCamera:
            title = "Нет доступа к камере."
            message = "Пожалуйста, перейдите в настройки и разрешите доступ к камере."
        case .noRightsForLibrary:
            title = "Нет доступа к вашим фото."
            message = "Пожалуйста, перейдите в настройки и разрешите доступ к фото."
        }
        
        showAlert(with: title,
                  message: message)
    }
    
}

extension AddPlaceViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        view.endEditing(true)
        resetErrorLabel()
        
        guard let viewModel = viewModel else {
            return
        }
        let index = indexPath.item
        
        if viewModel.photoImages[index].isPlaceholder {
            showSourceSheet()
        } else {
            presentImagePreview(index: index)
        }
    }
}


extension AddPlaceViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let viewModel = viewModel else {
            return CGSize(width: 0, height: 0)
        }
        
        guard !viewModel.photoImages[indexPath.row].isPlaceholder else {
            return CGSize(width: 60, height: 60)
        }
        
        let photoImage = viewModel.photoImages[indexPath.row]
        let aspectRatio = photoImage.image.size.height / photoImage.image.size.width
        let height = CGFloat(100)
        let width = height / aspectRatio
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if collectionView.numberOfItems(inSection: 0) == 1 {
            let collectionViewWidth = collectionView.bounds.width
            
            let leftInset = (collectionViewWidth - 60) / 2
            let rightInset = leftInset
            return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        }
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}
