//
//  PhotoCollectionViewCell.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 08.12.2020.
//

import UIKit

struct PhotoImage: Hashable {
    let image: UIImage
    let isPlaceholder: Bool
    
    static func createPlaceholder() -> Self {
        return PhotoImage(image: UIImage(named: "AddPhoto")!, isPlaceholder: true)
    }
}

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
