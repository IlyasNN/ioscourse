//
//  ImagePreviewView.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 09.12.2020.
//

import UIKit

class ImagePreviewView: UIView {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var backgroundView: UIView!
    
    public var okHandler: (() -> ())?
    public var deleteHandler: (() -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        viewInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        viewInit()
    }
    
    private func viewInit() {
        configureUI()
    }
    
    private func configureUI() {

    }
    
    func setup(image: UIImage?) {
        imageView.image = image
    }
    
    @IBAction private func deleteButtonTapped(_ sender: UIButton) {
        deleteHandler?()
    }
    
    @IBAction func shadowTapped(_ sender: UITapGestureRecognizer) {
        self.removeFromSuperview()
    }
}
