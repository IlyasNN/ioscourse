//
//  AddPlaceViewModel.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 30.11.2020.
//

import UIKit

class AddPlaceViewModel: BaseViewModel {
    var name: String?
    var descriptionPlace: String?
    var latitude: Double
    var longitude: Double
    let descriptionTextViewPlaceHolder = "Описание места"
    let defaultTextViewHeight: Float = 50
    var photoImages: [PhotoImage] = []
    let imagesMaxCount = 5
    var cellHorizontalSpace: Float = 10
    
    private var error: String? {
        didSet {
            if let error = error {
                didGetError?(error)
            }
        }
    }
    
    var didSuccessfullySaved: (() -> Void)?
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
        super.init()
        addPlaceholderImage()
    }
}

extension AddPlaceViewModel: AddPlaceViewModeling {
    
    func savePlace() {
        isLoading = true
        
        let placeId = UUID().uuidString
        
        let filteredImages = self.photoImages.filter {
            !$0.isPlaceholder
        }
        let images = filteredImages.map {
            $0.image
        }
        
        StorageService.shared.savePlacePhoto(placeId: placeId, images: images) { [self] (result) in
            switch result {
            case .success(let imagePaths):
                print(imagePaths)
                let placeUid = UUID().uuidString
                FirestoreService.shared.savePlace(withUid: placeUid, name: name, descriptionPlace: descriptionPlace, latitude: latitude, longitude: longitude, imagePaths: imagePaths) { [weak self] (result) in
                    switch result {
                    case .success(_):
                        
                        guard let pilotUid = CurrentUserManager.shared.currentUser?.value?.uid else {
                            return
                        }
                        FirestoreService.shared.addWork(pilotUid: pilotUid, workId: placeUid) { (_) in
                            return
                        }
                        self?.didSuccessfullySaved?()
            
                    case .failure(let error):
                        self?.didGetError?(error.localizedDescription)
                        
                    }
                    self?.isLoading = false
                }
            case .failure(let error):
                self.didGetError?(error.localizedDescription)
            }
        }
    }
    
    
    var isSavingAvailable: Bool {
        (name?.isEmpty ?? true == false) &&
            (descriptionPlace?.isEmpty ?? true == false)
    }
    
    func addImage(image: UIImage) {
        photoImages.insert(PhotoImage(image: image, isPlaceholder: false), at: 1)
        if photoImages.count > imagesMaxCount {
            photoImages.removeFirst()
        }
        self.didChange?()
    }
    
    func removeImage(at index: Int) {
        photoImages.remove(at: index)
        if photoImages.count == imagesMaxCount - 1 &&
            photoImages.first?.isPlaceholder == false {
            addPlaceholderImage()
        }
        self.didChange?()
    }
    
    private func addPlaceholderImage() {
        photoImages.insert(PhotoImage.createPlaceholder(), at: 0)
    }
}
