//
//  PlaceViewModel.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 10.12.2020.
//

import UIKit

class PlaceViewModel: BaseViewModel {
    var placeModel: PlaceModel
    var pilotModel: PilotModel?
    var photoImages: [UIImage] = []
    
    init(placeModel: PlaceModel) {
        
        self.placeModel = placeModel
        super.init()
        
        isLoading = true
        
        FirestoreService.shared.getPilotData(uid: placeModel.ownerId) { (result) in
            
            switch result {
            case .success(let pilotModel):
                self.pilotModel = pilotModel
                self.didChange?()
            case .failure(let error):
                self.didGetError?(error.localizedDescription)
            }
        }
        
        StorageService.shared.getPlacePhotos(photoPaths: placeModel.imagePaths) { [weak self] (result) in
            switch result {
            case .success(let images):
                self?.photoImages = images
                self?.isLoading = false
                self?.didChange?()
            case .failure(let error):
                self?.didGetError?(error.localizedDescription)
            }
        }
    }
}

extension PlaceViewModel: PlaceViewModeling {
    
}
