//
//  PlaceViewController.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 10.12.2020.
//

import UIKit
import MapKit

protocol PlaceRouting: BaseRouting {
    func navigateBack(_ completion: (() -> Void)?)
}

protocol PlaceViewModeling: BaseViewModel {
    var placeModel: PlaceModel { get }
    var pilotModel: PilotModel? { get }
    var photoImages: [UIImage] { get }
}

fileprivate enum Section {
    case main
}

fileprivate typealias DataSource = UICollectionViewDiffableDataSource<Section, UIImage>
fileprivate typealias DataSourceSnapshot = NSDiffableDataSourceSnapshot<Section, UIImage>

class PlaceViewController: UIViewController {
    
    @IBOutlet weak var namePlace: UILabel!
    @IBOutlet weak var mapSnapshotImageView: UIImageView!
    @IBOutlet weak var descriptionPlaceLabel: UILabel!
    @IBOutlet weak var nameSurnameLabel: UILabel!
    @IBOutlet weak var photoImagesCollectionView: UICollectionView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var router: PlaceRouting?
    var viewModel: PlaceViewModeling? {
        didSet {
            viewModel?.didChange = { [weak self] in
                DispatchQueue.main.async { [weak self] in
                    self?.update()
                }
            }
        }
    }
    
    private lazy var dataSource = setupDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        updateDataSource()
    }
    
    private func setup() {
        namePlace.text = viewModel?.placeModel.name
        descriptionPlaceLabel.text = viewModel?.placeModel.descriptionPlace
        nameSurnameLabel.text = ""
        
        setupMapSnapshot()
        
        // CollectionView
        photoImagesCollectionView.dataSource = dataSource
        let reuseId = PlaceImageCollectionViewCell.typeName
        photoImagesCollectionView.register(UINib(nibName: reuseId, bundle: nil),
                                forCellWithReuseIdentifier: reuseId)
    }
    
    private func setupMapSnapshot() {
        guard let placeModel = viewModel?.placeModel else {
            return
        }
        
        let coords = CLLocationCoordinate2D(latitude: placeModel.latitude, longitude: placeModel.longitude)
        
        let options = MKMapSnapshotter.Options()
        options.region = MKCoordinateRegion(center: coords , latitudinalMeters: 500, longitudinalMeters: 500)
        options.size = CGSize(width: 400, height: 200)
        let snapShotter = MKMapSnapshotter(options: options)
        
        snapShotter.start { (snapshot, error) in
            guard error == nil, let snapshot = snapshot else {
                return
            }
            self.mapSnapshotImageView.image = snapshot.image
            
        }
    }
    
    private func setupDataSource() -> DataSource {
        let dataSource = DataSource(
            collectionView: photoImagesCollectionView,
            cellProvider: { (collectionView, indexPath, image) ->
                UICollectionViewCell? in
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: PlaceImageCollectionViewCell.typeName,
                    for: indexPath) as? PlaceImageCollectionViewCell
                cell?.imageView.image = image
                return cell
            })
        return dataSource
    }
    
    private func update() {
        guard isViewLoaded else {
            return
        }
        
        updateDataSource()
        
        nameSurnameLabel.text = "автор: \(viewModel?.pilotModel?.nameSurname ?? "")"
        
        let isLoading = viewModel?.isLoading ?? true
        if isLoading {
            loadingIndicator.startAnimating()
            loadingIndicator.isHidden = false
        } else {
            loadingIndicator.isHidden = true
            loadingIndicator.stopAnimating()
        }
    }
    
    private func updateDataSource(animatingDifferences: Bool = true) {
        guard let viewModel = viewModel else {
            return
        }
        var snapshot = DataSourceSnapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(viewModel.photoImages, toSection: .main)
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        router?.navigateBack(nil)
    }
}

extension PlaceViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard let viewModel = viewModel else {
            return CGSize(width: 0, height: 0)
        }
        
        let photoImage = viewModel.photoImages[indexPath.row]
        let aspectRatio = photoImage.size.height / photoImage.size.width
        let width = collectionView.frame.size.width
        let height = width * aspectRatio
        
        return CGSize(width: width, height: height)
    }
}
