//
//  PlaceImageCollectionViewCell.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 18.12.2020.
//

import UIKit

class PlaceImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}
