//
//  MainTabBarRouter.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 06.11.2020.
//

import Foundation

final class MainTabBarRouter: BaseRouter {
    
    weak var mainTabBarViewController: MainTabBarViewController?

    var mapFlowRouter: MapFlowRouting?
    var pilotsFlowRouter: PilotsFlowRouting?
    var profileFlowRouter: ProfileFlowRouting?
 
    init(with mainTabBarViewController: MainTabBarViewController) {
        self.mainTabBarViewController = mainTabBarViewController
        
        super.init()
        
        self.mapFlowRouter = MapFlowRouter(with: self)
        self.pilotsFlowRouter = PilotsFlowRouter(with: self)
        self.profileFlowRouter = ProfileFlowRouter(with:self)
    }
   
}

extension MainTabBarRouter: MainTabBarRouting {
    
    func presentWelcomeFlowRouter(_ completion: (() -> Void)?) {
        let welcomeVC = WelcomeViewController.initFromItsStoryboard()
        let router = AuthorizationFlowRouter(initialViewController: welcomeVC)
        router.delegate = self
        welcomeVC.router = router
        welcomeVC.modalPresentationStyle = .overFullScreen
        mainTabBarViewController?.present(welcomeVC, animated: false) {
            self.mapFlowRouter?.dissmiss(nil)
            self.pilotsFlowRouter?.dissmiss(nil)
            self.profileFlowRouter?.dissmiss(nil)
        }
        
//        let welcomeVC = WelcomeViewController.initFromItsStoryboard()
//        let router = WelcomeFlowRouter(initialViewController: welcomeVC)
//        router.delegate = self
//        welcomeVC.router = router
//        welcomeVC.modalPresentationStyle = .overFullScreen
//        self.notificationsFlowRouter.dissmiss(nil)
//        self.shoppingListFlowRouter.dissmiss(nil)
//        self.userProfileFlowRouter.dissmiss() {
//            self.mainTabBarViewController?.present(welcomeVC, animated: false)
//        }
    }
    
    func reset() {
        mapFlowRouter?.presentMapViewController(nil) 
        pilotsFlowRouter?.presentPilotsViewController(nil)
        profileFlowRouter?.presentProfileViewController(nil)
    }
    
}

extension MainTabBarRouter: AuthorizationFlowRouterDelegate {
    func dissmissWelcomeFlow() {
        reset()
    }
}

