//
//  BecomePilotViewModel.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 12.12.2020.
//

import UIKit

class BecomePilotViewModel: BaseViewModel {
    
    var phoneNumber: String? {
        didSet {
            didChange?()
        }
    }
    var copterName: String? {
        didSet {
            didChange?()
        }
    }
    var profileImage: UIImage? {
        didSet {
            didChange?()
        }
    }
    
    let copterTypes = ["Не указано",
                       "DJI mavic Air",
                       "DJI Phantom 4",
                       "DJI spark",
                       "DJI mavic Pro",
                       "Xiaomi",
                       "Неизвестный производитель"]
    
    var didSuccessfullyBecamePilot: (() -> Void)?
    
}

extension BecomePilotViewModel: BecomePilotViewModeling {
    
    var isConfirmationAvailable: Bool {
        (phoneNumber?.isEmpty ?? true == false) &&
            (copterName?.isEmpty ?? true == false) &&
            (profileImage != nil)
    }

    func becomePilot() {
        
        isLoading = true
        
        guard let phoneNumber = phoneNumber,
              let copterName = copterName,
              let profileImage = profileImage else {
            didGetError?("Заполните все поля и выберите фото")
            return
        }
        
        CurrentUserManager.shared.becomePilot(phoneNumber: phoneNumber, copterName: copterName, profileImage: profileImage) { [weak self] (result) in
            switch result {
            case .success(_):
                self?.didSuccessfullyBecamePilot?()
            case .failure(let error):
                self?.didGetError?(error.localizedDescription)
            }
            self?.isLoading = false
        }
    }
    
}
