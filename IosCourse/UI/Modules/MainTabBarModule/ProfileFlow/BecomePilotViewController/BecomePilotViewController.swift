//
//  BecomePilotViewController.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 12.12.2020.
//

import UIKit
import PhoneNumberKit

protocol BecomePilotRouting: BaseRouting {
    func presentProfileViewController(_ completion: (()->Void)?)
}

protocol BecomePilotViewModeling: BaseViewModeling {
    
    var phoneNumber: String? { get set }
    var copterName: String? { get set }
    var profileImage: UIImage? { get set }
    var isConfirmationAvailable: Bool { get }
    var didSuccessfullyBecamePilot: (() -> Void)? { get set }
    
    var copterTypes: [String] { get }
    
    func becomePilot()
}

class RuPhoneNumberTextField: PhoneNumberTextField {
    override var defaultRegion: String {
        get {
            return "RU"
        }
        set {} // exists for backward compatibility
    }
}


class BecomePilotViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var phoneNumberTextField: RuPhoneNumberTextField!
    @IBOutlet weak var copterTypePicker: UIPickerView!
    @IBOutlet weak var confirmButton: DesignableButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var router: BecomePilotRouting?
    var viewModel: BecomePilotViewModeling? {
        didSet {
            viewModel?.didChange = { //[weak self] in
                DispatchQueue.main.async { [weak self] in
                    self?.update()
                }
            }
            
            viewModel?.didSuccessfullyBecamePilot = { [weak self] in
                DispatchQueue.main.async {
                    self?.router?.presentProfileViewController(nil)
                }
            }
        }
    }
    
    private let imagePickerService = ImagePickerService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        update()
    }
    
    private func setup() {
        
        //PhoneNumberTextField
        phoneNumberTextField.withExamplePlaceholder = true
        
        //ScrollView
        scrollView.isUserInteractionEnabled = true
        scrollView.isExclusiveTouch = true
        scrollView.canCancelContentTouches = true
        scrollView.delaysContentTouches = true
        
        // ImagePickerService
        imagePickerService.delegate = self
        imagePickerService.presentingViewController = self
        
    }
    
    private func update() {
        guard isViewLoaded else {
            return
        }
        updateConfirmButton()
        updateLoadingState()
    }
    
    private func updateConfirmButton() {
        confirmButton.isEnabled = viewModel?.isConfirmationAvailable ?? false
    }
    
    private func updateLoadingState() {
        let isLoading = viewModel?.isLoading ?? false
        confirmButton.isHidden = isLoading
        loadingIndicator.isHidden = isLoading == false
        isLoading ? loadingIndicator.startAnimating() : loadingIndicator.stopAnimating()
    }
    
    func showSourceSheet() {
        let alertVC = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertVC.addAction(UIAlertAction(title: "Сделать фото",
                                        style: .default,
                                        handler: { [weak self] _ in
                                            self?.imagePickerService.pickPhotoFromCamera(allowsEditing: true)
        }))
        
        alertVC.addAction(UIAlertAction(title: "Выбрать из галереи",
                                        style: .default,
                                        handler: { [weak self] _ in
            self?.imagePickerService.pickPhotoFromLibrary(allowsEditing: true)
        }))
        
        alertVC.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        present(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        viewModel?.phoneNumber = sender.text
    }
    
    @IBAction func profileImageTapped(_ sender: Any) {
        showSourceSheet()
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        router?.presentProfileViewController(nil)
    }
    
    @IBAction func approveButtonTapped(_ sender: Any) {
        viewModel?.becomePilot()
    }
    
}

extension BecomePilotViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel?.copterTypes.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewModel?.copterTypes[row]
    }
}

extension BecomePilotViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        viewModel?.copterName = viewModel?.copterTypes[row]
    }
}

extension BecomePilotViewController: ImagePickerServiceDelegate {
    
    func imagePickerService(finishWith image: UIImage) {
        viewModel?.profileImage = image
        // TOBE deleted
        profileImageView.image = image
    }
    
    func imagePickerService(finishWith error: ImagePickerError) {
        
        let title: String
        let message: String
        
        switch error {
        case .noRightsForCamera:
            title = "Нет доступа к камере."
            message = "Пожалуйста, перейдите в настройки и разрешите доступ к камере."
        case .noRightsForLibrary:
            title = "Нет доступа к вашим фото."
            message = "Пожалуйста, перейдите в настройки и разрешите доступ к фото."
        }
        
        showAlert(with: title,
                  message: message)
    }
    
}
