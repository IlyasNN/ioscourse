//
//  SecondScreenFlowRouter.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 06.11.2020.
//

import Foundation

public protocol ProfileFlowRouting: class, BaseRouting {
    func presentProfileViewController(_ completion: (()->Void)?)
    func dissmiss(_ completion: (() -> Void)?)
}

final class ProfileFlowRouter: BaseRouter {
    
    weak var mainTabBarRouter: MainTabBarRouting?
    
    init(with mainTabBarRouter: MainTabBarRouting) {
        self.mainTabBarRouter = mainTabBarRouter
        
        super.init()
    }
    
    func dissmiss(_ completion: (() -> Void)?) {
        let cleanVC = CleanViewController.initFromItsStoryboard()
        navigationController.setViewControllers([cleanVC], animated: false)
    }
}

extension ProfileFlowRouter: ProfileFlowRouting {
    
    func presentProfileViewController(_ completion: (() -> Void)?) {
        if let presentedRVC = navigationController.viewControllers.filter({ $0 is ProfileViewController}).first {
            navigationController.popToViewController(presentedRVC, animated: true)
        } else {
            let vc = ProfileViewController.initFromItsStoryboard()
            vc.viewModel = ProfileViewModel()
            vc.router = self
            vc.modalPresentationStyle = .fullScreen
            
            navigationController.viewControllers.removeAll()
            if navigationController.viewControllers.count == 0 {
                navigationController.viewControllers.append(vc)
            } else {
                navigationController.pushViewController(vc, animated: true)
            }
        }
    }
    
}

extension ProfileFlowRouter: ProfileRouting {
    
    func presentBecomePilotViewController(_ completion: (() -> Void)?) {
        if let presentedRVC = navigationController.viewControllers.filter({ $0 is BecomePilotViewController}).first {
            navigationController.popToViewController(presentedRVC, animated: true)
        } else {
            let vc = BecomePilotViewController.initFromItsStoryboard()
            vc.viewModel = BecomePilotViewModel()
            vc.router = self
            vc.modalPresentationStyle = .fullScreen
            
            if navigationController.viewControllers.count == 0 {
                navigationController.viewControllers.append(vc)
            } else {
                navigationController.pushViewController(vc, animated: true)
            }
        }
    }
    
    func presentWelcomeViewController(_ completion: (() -> Void)?) {
        mainTabBarRouter?.presentWelcomeFlowRouter(nil)
    }
    
}

extension ProfileFlowRouter: BecomePilotRouting {
    
}

