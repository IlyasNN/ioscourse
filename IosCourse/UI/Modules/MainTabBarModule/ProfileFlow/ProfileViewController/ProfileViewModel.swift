//
//  SecondScreenViewModel.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 06.11.2020.
//

import Foundation

class ProfileViewModel: BaseViewModel {
    
    private var currentUserEmitter: Emitter<CurrentUserModel?>? = CurrentUserManager.shared.currentUser
    private var currentUserSubscriptionToken: SignalSubscriptionToken? = nil
    
    var currentUserModel: CurrentUserModel? {
        didSet {
            self.didChange?()
        }
    }
    
    override init() {
        super.init()
        self.currentUserSubscriptionToken = CurrentUserManager.shared.currentUser?.signal.addListener { [weak self] (currentUser) in
            self?.currentUserModel = self?.currentUserEmitter?.value
        }
    }
    
    deinit {
        self.currentUserEmitter?.signal.removeListener(currentUserSubscriptionToken)
    }
    
}

extension ProfileViewModel: ProfileViewModeling {
    
}
