//
//  SecondScreenViewController.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 06.11.2020.
//

import UIKit

protocol ProfileRouting: BaseRouting {
    func presentWelcomeViewController(_ completion: (() -> Void)?)
    func presentBecomePilotViewController(_ completion: (() -> Void)?)
}

protocol ProfileViewModeling: BaseViewModeling {
    var currentUserModel: CurrentUserModel? { get }
}

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var firstnameLabel: UILabel!
    @IBOutlet weak var lastnameLabel: UILabel!
    @IBOutlet weak var phoneStackView: UIStackView!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var becomePilotButton: DesignableButton!
    
    var router: ProfileRouting?
    var viewModel: ProfileViewModeling? {
        didSet {
            viewModel?.didChange = { [weak self] in
                DispatchQueue.main.async { [weak self] in
                    self?.update()
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        update()
    }
    
    private func update() {
        guard let viewModel = viewModel, isViewLoaded else {
            return
        }
        firstnameLabel.text = viewModel.currentUserModel?.firstName
        lastnameLabel.text = viewModel.currentUserModel?.lastName
        emailLabel.text = viewModel.currentUserModel?.email
        let isPilot = viewModel.currentUserModel?.isPilot ?? false
        becomePilotButton.isHidden = isPilot
        profileImageView.isHidden = !isPilot
        profileImageView.image = viewModel.currentUserModel?.pilotModel?.profileImage
        phoneStackView.isHidden = !isPilot
        phoneLabel?.text = viewModel.currentUserModel?.pilotModel?.phoneNumber
    }
    
    @IBAction func logoutButtonTapped(_ sender: Any) {
        CurrentUserManager.shared.logOut {
            self.router?.presentWelcomeViewController(nil)
        }
    }
    
    @IBAction func becomePilotButtonTapped(_ sender: Any) {
        router?.presentBecomePilotViewController(nil)
    }
    
}
