//
//  MainTabBarViewController.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 06.11.2020.
//

import UIKit

public protocol MainTabBarRouting: class, BaseRouting {
    var pilotsFlowRouter: PilotsFlowRouting? { get }
    var mapFlowRouter: MapFlowRouting? { get }
    var profileFlowRouter: ProfileFlowRouting? { get }
    
    func reset()
    func presentWelcomeFlowRouter(_ completion: (() -> Void)?)
   // func dissmiss(_ completion: (() -> Void)?)
}

final class MainTabBarViewController: BaseViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tabBar: UITabBar!
    
    lazy var router: MainTabBarRouting = {
        MainTabBarRouter(with: self)
    }()
    
    var viewControllers: [UIViewController]!
    var selectedIndex: Int = 0 {
        didSet {
            let previousVC = viewControllers[oldValue]
            previousVC.willMove(toParent: nil)
            previousVC.view.removeFromSuperview()
            previousVC.removeFromParent()
            
            let vc = viewControllers[selectedIndex]
            addChild(vc)
            vc.view.frame = contentView.bounds
            contentView.addSubview(vc.view)
            vc.didMove(toParent: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBar.delegate = self
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if CurrentUserManager.shared.isLoggedIn.value {
            router.reset()
        } else {
            router.presentWelcomeFlowRouter(nil)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setup() {
        
        guard let mapFlowRouter = router.mapFlowRouter,
              let pilotsFlowRouter = router.pilotsFlowRouter,
              let profileFlowRouter = router.profileFlowRouter else {
            return
        }
        
        viewControllers = [
            mapFlowRouter.navigationController,
            pilotsFlowRouter.navigationController,
            profileFlowRouter.navigationController
        ]
        
        tabBar.selectedItem = tabBar.items?.first
        selectedIndex = 0
    }

}

extension MainTabBarViewController: UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let index = tabBar.items?.firstIndex(of: item)
        
        selectedIndex = index ?? 0
    }
}
