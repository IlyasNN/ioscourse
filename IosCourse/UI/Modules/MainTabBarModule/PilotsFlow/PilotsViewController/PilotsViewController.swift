//
//  PilotsViewController.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 12.12.2020.
//

import UIKit

protocol PilotsRouting: BaseRouting {
    func presentWorksViewController(pilotModel: PilotModel, _ completion: (() -> Void)?)
}

protocol PilotsViewModeling: BaseViewModeling {
    var pilotsModels: [PilotModel] { get }
}

fileprivate enum Section {
    case main
}

fileprivate typealias DataSource = UITableViewDiffableDataSource<Section, PilotModel>
fileprivate typealias DataSourceSnapshot = NSDiffableDataSourceSnapshot<Section, PilotModel>

class PilotsViewController: UIViewController {

    @IBOutlet weak var pilotsTableView: UITableView!
    
    var router: PilotsRouting?
    var viewModel: PilotsViewModeling? {
        didSet {
            viewModel?.didChange = { [weak self] in
                DispatchQueue.main.async { [weak self] in
                    self?.update()
                }
            }
            
            viewModel?.didGetError = { [weak self] message in
                DispatchQueue.main.async { [weak self] in
                    self?.showAlert(message: message)
                }
            }
        }
    }
    
    private lazy var dataSource = setupDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TableView
        pilotsTableView.dataSource = dataSource
        let reuseId = PilotTableViewCell.typeName
        pilotsTableView.register(UINib(nibName: reuseId, bundle: nil), forCellReuseIdentifier: reuseId)
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        update()
    }
    
    private func setup() {
        
    }
    
    private func setupDataSource() -> DataSource {
        let dataSource = DataSource(
            tableView: pilotsTableView,
            cellProvider: { [weak self] (tableView, indexPath, pilotModel) ->
                UITableViewCell? in
                
                let cell = tableView.dequeueReusableCell(
                    withIdentifier: PilotTableViewCell.typeName,
                    for: indexPath) as? PilotTableViewCell
                cell?.profileImage.image = pilotModel.profileImage
                cell?.nameSurnameLabel.text = pilotModel.nameSurname
                cell?.copterTypeLabel.text = pilotModel.copterName
                cell?.numberOfWorksLabel.text = "Кол-во работ: \(pilotModel.worksIds.count)"
                cell?.phoneNumber = pilotModel.phoneNumber
                cell?.delegate = self
                return cell
            })
        return dataSource
    }
    
    private func update() {
        guard isViewLoaded else {
            return
        }
        updateDataSource()
    }
    
    private func updateDataSource(animatingDifferences: Bool = true) {
        guard let viewModel = viewModel else {
            return
        }
        var snapshot = DataSourceSnapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(viewModel.pilotsModels, toSection: .main)
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }

}


extension PilotsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let pilotModel = viewModel?.pilotsModels[indexPath.row] else {
            return
        }
        router?.presentWorksViewController(pilotModel: pilotModel, nil)
    }
    
}

extension PilotsViewController: PilotTableViewCellDelegate {
    func CallButtonTapped(with phone: String) {
        let clearedPhone = phone.replacingOccurrences(of: "-", with: "")
            .replacingOccurrences(of: " ", with: "")
        if let phoneCallURL = URL(string: "tel://\(clearedPhone)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
}

