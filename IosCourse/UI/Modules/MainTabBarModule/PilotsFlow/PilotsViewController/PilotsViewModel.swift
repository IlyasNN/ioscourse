//
//  PilotsViewModel.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 12.12.2020.
//

import Foundation

class PilotsViewModel: BaseViewModel {
    
    var pilotsModels: [PilotModel] = [] {
        didSet {
            self.didChange?()
        }
    }
    
    override init() {
        
        super.init()
        
        FirestoreService.shared.getPilotsData { [weak self] (result) in
            switch result {
            case .success(let pilots):
                self?.pilotsModels = pilots
            case .failure(let error):
                self?.didGetError?(error.localizedDescription)
            }
        }
    }
    
}

extension PilotsViewModel: PilotsViewModeling {
    
}
