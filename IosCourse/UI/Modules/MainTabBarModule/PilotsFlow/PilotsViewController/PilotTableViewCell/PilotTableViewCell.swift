//
//  PilotTableViewCell.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 14.12.2020.
//

import UIKit

protocol PilotTableViewCellDelegate: AnyObject {
    func CallButtonTapped(with phone: String)
}

class PilotTableViewCell: UITableViewCell {
    
    weak var delegate: PilotTableViewCellDelegate?

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameSurnameLabel: UILabel!
    @IBOutlet weak var copterTypeLabel: UILabel!
    @IBOutlet weak var numberOfWorksLabel: UILabel!
    var phoneNumber: String? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func callButtonTapped(_ sender: Any) {
        guard let phoneNumber = phoneNumber else {
            return
        }
        delegate?.CallButtonTapped(with: phoneNumber)
    }
    
}
