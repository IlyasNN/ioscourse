//
//  WorkTableViewCell.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 14.12.2020.
//

import UIKit

class WorkTableViewCell: UITableViewCell {

    @IBOutlet weak var placeImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var descriptionPlace: UILabel!
    @IBOutlet weak var nameSurnamePilot: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
