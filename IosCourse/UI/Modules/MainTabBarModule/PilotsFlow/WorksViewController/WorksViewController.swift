//
//  WorksViewController.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 14.12.2020.
//

import UIKit

protocol WorksRouting: BaseRouting {
    func presentPilotsViewController(_ completion: (() -> Void)?)
    func presentPlaceViewController(placeModel: PlaceModel, _ completion: (() -> Void)?)
}

protocol WorksViewModeling: BaseViewModeling {
    var worksModels: [WorkModel] { get }
}

fileprivate enum Section {
    case main
}

fileprivate typealias DataSource = UITableViewDiffableDataSource<Section, WorkModel>
fileprivate typealias DataSourceSnapshot = NSDiffableDataSourceSnapshot<Section, WorkModel>

class WorksViewController: UIViewController {

    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var worksTableView: UITableView!
    
    var router: WorksRouting?
    var viewModel: WorksViewModeling? {
        didSet {
            viewModel?.didChange = { [weak self] in
                DispatchQueue.main.async { [weak self] in
                    self?.update()
                }
            }
            
            viewModel?.didGetError = { [weak self] message in
                DispatchQueue.main.async { [weak self] in
                    self?.showAlert(message: message)
                }
            }
        }
    }
    
    private lazy var dataSource = setupDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TableView
        worksTableView.dataSource = dataSource
        let reuseId = WorkTableViewCell.typeName
        worksTableView.register(UINib(nibName: reuseId, bundle: nil), forCellReuseIdentifier: reuseId)
        setup()
    }
    
    private func setup() {
        
    }
    
    private func setupDataSource() -> DataSource {
        let dataSource = DataSource(
            tableView: worksTableView,
            cellProvider: { (tableView, indexPath, workModel) ->
                UITableViewCell? in
                
                let cell = tableView.dequeueReusableCell(
                    withIdentifier: WorkTableViewCell.typeName,
                    for: indexPath) as? WorkTableViewCell

                cell?.name.text = workModel.placeModel.name
                cell?.descriptionPlace.text = workModel.placeModel.descriptionPlace
                cell?.nameSurnamePilot.text = workModel.pilotModel.nameSurname
                cell?.placeImage.image = workModel.image
                
                return cell
            })
        return dataSource
    }
    
    private func update() {
        guard isViewLoaded else {
            return
        }
        
        updateDataSource()
        
        let isLoading = viewModel?.isLoading ?? true
        if isLoading {
            loadingIndicator.startAnimating()
            loadingIndicator.isHidden = false
        } else {
            loadingIndicator.isHidden = true
            loadingIndicator.stopAnimating()
        }
    }
    
    private func updateDataSource(animatingDifferences: Bool = true) {
        guard let viewModel = viewModel else {
            return
        }
        var snapshot = DataSourceSnapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(viewModel.worksModels, toSection: .main)
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        router?.presentPilotsViewController(nil)
    }
}

extension WorksViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let workModel = viewModel?.worksModels[indexPath.row] else {
            return
        }
        router?.presentPlaceViewController(placeModel: workModel.placeModel, nil)
    }
    
}
