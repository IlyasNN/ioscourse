//
//  WorksViewModel.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 14.12.2020.
//

import Foundation

class WorksViewModel: BaseViewModel {
    
    let pilotModel: PilotModel
    var worksModels: [WorkModel] = [] 
    
    init(pilotModel: PilotModel) {
        
        self.pilotModel = pilotModel
        
        super.init()
        
        isLoading = true
        
        FirestoreService.shared.getWorks(ownerId: pilotModel.uid) { [weak self] (result) in
            switch result {
            case .success(let works):
                self?.worksModels = works
                self?.isLoading = false
            case .failure(let error):
                self?.didGetError?(error.localizedDescription)
            }
        }
    }
}

extension WorksViewModel: WorksViewModeling {
    
}
