//
//  PilotsFlowRouter.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 12.12.2020.
//

import Foundation

public protocol PilotsFlowRouting: class, BaseRouting {
    func presentPilotsViewController(_ completion: (()->Void)?)
    func dissmiss(_ completion: (() -> Void)?)
}

final class PilotsFlowRouter: BaseRouter {
    
    weak var mainTabBarRouter: MainTabBarRouting?
    
    init(with mainTabBarRouter: MainTabBarRouting) {
        self.mainTabBarRouter = mainTabBarRouter
        
        super.init()
    }
    
    func dissmiss(_ completion: (() -> Void)?) {
        let cleanVC = CleanViewController.initFromItsStoryboard()
        navigationController.setViewControllers([cleanVC], animated: false)
    }
}

extension PilotsFlowRouter: PilotsFlowRouting {
    
    func presentPilotsViewController(_ completion: (() -> Void)?) {
        if let presentedRVC = navigationController.viewControllers.filter({ $0 is PilotsViewController}).first {
            navigationController.popToViewController(presentedRVC, animated: true)
        } else {
            let vc = PilotsViewController.initFromItsStoryboard()
            vc.viewModel = PilotsViewModel()
            vc.router = self
            vc.modalPresentationStyle = .fullScreen
            
            navigationController.viewControllers.removeAll()
            if navigationController.viewControllers.count == 0 {
                navigationController.viewControllers.append(vc)
            } else {
                navigationController.pushViewController(vc, animated: true)
            }
        }
    }
    
}

extension PilotsFlowRouter: PilotsRouting {
    func presentWorksViewController(pilotModel: PilotModel, _ completion: (() -> Void)?) {
        if let presentedRVC = navigationController.viewControllers.filter({ $0 is WorksViewController}).first {
            navigationController.popToViewController(presentedRVC, animated: true)
        } else {
            let vc = WorksViewController.initFromItsStoryboard()
            vc.viewModel = WorksViewModel(pilotModel: pilotModel)
            vc.router = self
            vc.modalPresentationStyle = .fullScreen
            
            if navigationController.viewControllers.count == 0 {
                navigationController.viewControllers.append(vc)
            } else {
                navigationController.pushViewController(vc, animated: true)
            }
        }
    }

}

extension PilotsFlowRouter: WorksRouting {
    func presentPlaceViewController(placeModel: PlaceModel, _ completion: (() -> Void)?) {
        if let presentedRVC = navigationController.viewControllers.filter({ $0 is PlaceViewController}).first {
            navigationController.popToViewController(presentedRVC, animated: true)
        } else {
            let vc = PlaceViewController.initFromItsStoryboard()
            vc.viewModel = PlaceViewModel(placeModel: placeModel)
            vc.router = self
            vc.modalPresentationStyle = .fullScreen
            
            if navigationController.viewControllers.count == 0 {
                navigationController.viewControllers.append(vc)
            } else {
                navigationController.pushViewController(vc, animated: true)
            }
        }
        completion?()
    }
}

extension PilotsFlowRouter: PlaceRouting {
    func navigateBack(_ completion: (() -> Void)?) {
        navigationController.popViewController(animated: true)
    }
    
    
}


