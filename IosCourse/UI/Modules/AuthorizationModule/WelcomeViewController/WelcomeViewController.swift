//
//  WelcomeViewController.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 10.11.2020.
//

import UIKit

protocol WelcomeRouting {
    func presentSignUpViewController(_ completion: (()->Void)?)
    func presentLoginViewController(_ completion: (()->Void)?)
}

class WelcomeViewController: UIViewController {

    lazy var router: WelcomeRouting? = {
        AuthorizationFlowRouter(initialViewController: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func signUpButtonTapped(_ sender: Any) {
        router?.presentSignUpViewController(nil)
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        router?.presentLoginViewController(nil)
    }
    
}
