//
//  SignUpViewModel.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 10.11.2020.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

class SignUpViewModel: BaseViewModel {
    var firstName: String?
    var lastName: String?
    var email: String?
    var password: String?
    var repeatPassword: String?
    
    var didSuccessfullyRegistered: (() -> Void)?
}

extension SignUpViewModel: SignUpViewModeling {
    
    var isRegistrationAvailable: Bool {
        (firstName?.isEmpty ?? true == false) &&
            (lastName?.isEmpty ?? true == false) &&
            (email?.isEmpty ?? true == false) &&
            (password?.isEmpty ?? true == false)
    }
    
    func signUp() {
        
        isLoading = true
        
        AuthService.shared.register(withEmail: email, password: password, confirmPassword: repeatPassword) { [weak self] (result) in
            switch result {
            case .success(let user):
                FirestoreService.shared.saveProfile(withUid: user.uid, firstName: self?.firstName, lastName: self?.lastName, email: user.email!) { [weak self] (result) in
                    switch result {
                    case .success(let currentUserModel):
                        CurrentUserManager.shared.setUser(currentUserModel: currentUserModel)
                        self?.didSuccessfullyRegistered?()
                    case .failure(let error):
                        self?.didGetError?(error.localizedDescription)
                    }
                }
            case .failure(let error):
                self?.didGetError?(error.localizedDescription)
            }
            self?.isLoading = false
        }
    }
    
    
    private func validateFields() -> String? {
        
        if firstName?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            lastName?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            email?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            password?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Пожалуйста заполните все поля."
        }
        
        //check if the password is secure
        guard let cleanedPassword = password?.trimmingCharacters(in: .whitespacesAndNewlines),
              Validators.isPasswordValid(cleanedPassword) == true else {
            return "Убедитесь, что пароль состоит минимум из 8 символов, содержит специальный символ и заглваную букву."
        }
        
        return nil
    }
    
}
