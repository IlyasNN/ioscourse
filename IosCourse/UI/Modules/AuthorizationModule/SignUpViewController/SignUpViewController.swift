//
//  SignUpViewController.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 10.11.2020.
//

import UIKit

protocol SignUpRouting: BaseRouting {
    func presentWelcomeViewController(_ completion: (() -> Void)?)
    func presentTabBarViewController(_ completion: (() -> Void)?)
}

protocol SignUpViewModeling: BaseViewModeling {
    var firstName: String? { get set }
    var lastName: String? { get set }
    var email: String? { get set }
    var isRegistrationAvailable: Bool { get }
    var password: String? { get set }
    var repeatPassword: String? { get set }
    
    func signUp()
    
    var didSuccessfullyRegistered: (() -> Void)? { get set }
    
    var didGetError: ((String) -> Void)? { get set }
}

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var router: SignUpRouting?
    var viewModel: SignUpViewModeling? {
        didSet {
            
            viewModel?.didChange = { [weak self] in
                DispatchQueue.main.async { [weak self] in
                    self?.update()
                }
            }
            
            viewModel?.didSuccessfullyRegistered = { [weak self] in
                DispatchQueue.main.async {
                    self?.router?.presentTabBarViewController(nil)
                }
            }
            
            viewModel?.didGetError = { [weak self] message in
                DispatchQueue.main.async { [weak self] in
                    self?.updateErrorLabel(message)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        update()
    }
    
    @objc func keyboardWillShow(notification:NSNotification) {

        guard let userInfo = notification.userInfo else { return }
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)

        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 20
        scrollView.contentInset = contentInset
    }

    @objc func keyboardWillHide(notification:NSNotification) {

        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    private func setup() {
        guard isViewLoaded else {
            return
        }
        errorLabel.alpha = 0
        signUpButton.isEnabled = false
        loadingIndicator.isHidden = true
    }
    
    private func update() {
        guard isViewLoaded else {
            return
        }
        updateSignUpButton()
        updateLoadingState()
    }
    
    private func updateErrorLabel(_ message: String) {
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    private func resetErrorLabel() {
        errorLabel.alpha = 0
    }
    
    private func updateSignUpButton() {
        signUpButton.isEnabled = viewModel?.isRegistrationAvailable ?? false
    }
    
    private func updateLoadingState() {
        let isLoading = viewModel?.isLoading ?? false
        signUpButton.isHidden = isLoading
        loadingIndicator.isHidden = isLoading == false
        isLoading ? loadingIndicator.startAnimating() : loadingIndicator.stopAnimating()
    }
    
    @IBAction func scrollViewTapped(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        viewModel?.firstName = firstNameTextField.text
        viewModel?.lastName = lastNameTextField.text
        viewModel?.email = emailTextField.text
        viewModel?.password = passwordTextField.text
        viewModel?.repeatPassword = repeatPasswordTextField.text
        resetErrorLabel()
        update()
    }
    
    @IBAction func textFieldReturnButtonTapped(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        router?.presentWelcomeViewController(nil)
    }
    
    @IBAction func signUpButtonTapped(_ sender: Any) {
        resetErrorLabel()
        viewModel?.signUp()
    }
    
}
