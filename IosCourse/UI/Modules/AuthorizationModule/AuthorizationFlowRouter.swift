//
//  AuthorizationFlowRouter.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 10.11.2020.
//

import UIKit

protocol AuthorizationFlowRouterDelegate: class {
    func dissmissWelcomeFlow()
}

class AuthorizationFlowRouter: BaseRouter {
    
    weak var initialViewController: UIViewController?
    weak var delegate: AuthorizationFlowRouterDelegate?
    
    init(initialViewController: UIViewController) {
        self.initialViewController = initialViewController
        
        super.init()
        
        navigationController.view.backgroundColor = nil
    }
    
    func presentWelcomeViewController(_ completion: (() -> Void)?) {
        initialViewController?.dismiss(animated: true) {
            self.navigationController.viewControllers.removeAll()
            completion?()
        }
    }
    
}

extension AuthorizationFlowRouter: WelcomeRouting {
    
    func presentSignUpViewController(_ completion: (() -> Void)?) {
        if let presentedRVC = navigationController.viewControllers.filter({ $0 is SignUpViewController}).first {
            navigationController.popToViewController(presentedRVC, animated: true)
        } else {
            let vc = SignUpViewController.initFromItsStoryboard()
            vc.router = self
            vc.viewModel = SignUpViewModel()
            
            if navigationController.viewControllers.count == 0 {
                navigationController.viewControllers.append(vc)
            } else {
                navigationController.pushViewController(vc, animated: false)
            }
            
            navigationController.modalPresentationStyle = .overFullScreen
            initialViewController?.present(navigationController, animated: true, completion: completion)
        }
    }
    
    func presentLoginViewController(_ completion: (() -> Void)?) {
        if let presentedRVC = navigationController.viewControllers.filter({ $0 is LoginViewController}).first {
            navigationController.popToViewController(presentedRVC, animated: true)
        } else {
            let vc = LoginViewController.initFromItsStoryboard()
            
            vc.router = self
            vc.viewModel = LoginViewModel()
            
            if navigationController.viewControllers.count == 0 {
                navigationController.viewControllers.append(vc)
            } else {
                navigationController.pushViewController(vc, animated: false)
            }
            
            navigationController.modalPresentationStyle = .overFullScreen
            initialViewController?.present(navigationController, animated: true, completion: completion)
        }
    }
    
}

extension AuthorizationFlowRouter: LoginRouting {
    
}


extension AuthorizationFlowRouter: SignUpRouting {
    
    func presentTabBarViewController(_ completion: (() -> Void)?) {
        initialViewController?.presentedViewController?.dismiss(animated: true, completion: {
            self.initialViewController?.dismiss(animated: true, completion: {
                self.delegate?.dissmissWelcomeFlow()
                self.navigationController.viewControllers.removeAll()
            })
        })
    }

}


