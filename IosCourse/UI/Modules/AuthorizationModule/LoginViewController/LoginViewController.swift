//
//  LoginViewController.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 10.11.2020.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

protocol LoginRouting: BaseRouting {
    func presentWelcomeViewController(_ completion: (() -> Void)?)
    func presentTabBarViewController(_ completion: (() -> Void)?)
}

protocol LoginViewModeling: BaseViewModeling {
    var email: String? { get set }
    var isLoginAvailable: Bool { get }
    var password: String? { get set }
    
    func login()
    
    var didSuccessfullyLogedIn: (() -> Void)? { get set }
    
    var didGetError: ((String) -> Void)? { get set }
}

class LoginViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var router: LoginRouting?
    var viewModel: LoginViewModeling? {
        didSet {
            
            viewModel?.didChange = { [weak self] in
                DispatchQueue.main.async { [weak self] in
                    self?.update()
                }
            }
            
            viewModel?.didSuccessfullyLogedIn = { [weak self] in
                DispatchQueue.main.async {
                    self?.router?.presentTabBarViewController(nil)
                }
            }
            
            viewModel?.didGetError = { [weak self] message in
                DispatchQueue.main.async { [weak self] in
                    self?.updateErrorLabel(message)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        setup()
    }
    
    @objc func keyboardWillShow(notification:NSNotification) {

        guard let userInfo = notification.userInfo else { return }
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)

        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 20
        scrollView.contentInset = contentInset
    }

    @objc func keyboardWillHide(notification:NSNotification) {

        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    override func viewDidAppear(_ animated: Bool) {
        update()
    }
    
    private func setup() {
        guard isViewLoaded else {
            return
        }
        errorLabel.alpha = 0
        loginButton.isEnabled = false
        loadingIndicator.isHidden = true
    }
    
    private func update() {
        guard isViewLoaded else {
            return
        }
        updateLoginButton()
        updateLoadingState()
    }
    
    private func updateErrorLabel(_ message: String) {
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    private func resetErrorLabel() {
        errorLabel.alpha = 0
    }
    
    private func updateLoginButton() {
        loginButton.isEnabled = viewModel?.isLoginAvailable ?? false
    }
    
    private func updateLoadingState() {
        let isLoading = viewModel?.isLoading ?? false
        loginButton.isHidden = isLoading
        loadingIndicator.isHidden = isLoading == false
        isLoading ? loadingIndicator.startAnimating() : loadingIndicator.stopAnimating()
    }
    
    @IBAction func scrollViewTapped(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        viewModel?.email = emailTextField.text
        viewModel?.password = passwordTextField.text
        resetErrorLabel()
        update()
    }
    
    @IBAction func textFieldReturnButtonTapped(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        router?.presentWelcomeViewController(nil)
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        resetErrorLabel()
        viewModel?.login()
    }
    
}
