//
//  LoginViewModel.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 10.11.2020.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

class LoginViewModel: BaseViewModel {
    var email: String?
    var password: String?
    
    private var error: String? {
        didSet {
            if let error = error {
                didGetError?(error)
            }
        }
    }
    
    var didSuccessfullyLogedIn: (() -> Void)?
}

extension LoginViewModel: LoginViewModeling {
    
    var isLoginAvailable: Bool {
        (email?.isEmpty ?? true == false) &&
            (password?.isEmpty ?? true == false)
    }
    
    func login() {
        
        // Validate textFields
        guard let email = self.email?.trimmingCharacters(in: .whitespacesAndNewlines),
              let password = self.password?.trimmingCharacters(in: .whitespacesAndNewlines) else {
            didGetError?("Пожалуйста заполните все поля.")
            return
        }
        
        // Signing in user
        isLoading = true
        
        AuthService.shared.login(withEmail: email, password: password) { [weak self] (result) in
            switch result {
            case .success(let user):
                FirestoreService.shared.getUserData(uid: user.uid) { (result) in
                    switch result {
                    case .success(let currentUserModel):
                        CurrentUserManager.shared.setUser(currentUserModel: currentUserModel)
                        self?.didSuccessfullyLogedIn?()
                    case .failure(let error):
                        self?.didGetError?(error.localizedDescription)
                    }
                }
                
            case .failure(let error):
                self?.didGetError?(error.localizedDescription)
            }
            self?.isLoading = false
        }
        
    }
    
//    self?.loadUserData { data in
//        guard let firstName = data?["Firstname"] as? String,
//              let lastName = data?["Lastname"] as? String else {
//            self?.didGetError?("Ошибка получения данных пользователя.")
//            return
//        }
//
//        let currentUserModel = CurrentUserModel(uid: user.uid, firstName: firstName, lastName: lastName, email: email)
//        CurrentUserManager.shared.setUser(currentUserModel: currentUserModel)
//        self?.didSuccessfullyLogedIn?()
//    }
    
    func loadUserData(completion: ((_ data: [String : Any]?) -> Void)?) {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        let db = Firestore.firestore()
        let docRef = db.collection(FirebaseCodingKeys.users).document(uid)
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let data = document.data()
                completion?(data)
            } else {
                print("Document does not exist")
                completion?(nil)
            }
        }
        
    }
    
}

