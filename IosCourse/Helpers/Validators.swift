//
//  Validators.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 09.12.2020.
//

import Foundation

class Validators {
	static func isFilled(email: String?, password: String?, confirmPassword: String?) -> Bool {
		guard let password = password,
			let confirmPassword = confirmPassword,
			let email = email,
			!password.isEmpty, !confirmPassword.isEmpty, !email.isEmpty else {
				return false
		}
		return true
	}
	
	static func isFilled(firstName: String?, lastName: String?) -> Bool {
		guard let firstName = firstName,
			let lastName = lastName,
			!firstName.isEmpty, !lastName.isEmpty else {
				return false
		}
		return true
	}
	
	static func isSimpleEmail(_ email: String) -> Bool {
		let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
		
		let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
		return emailPred.evaluate(with: email)
	}
    
    static func isPasswordValid(_ password : String) -> Bool {
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }
}
