//
//  AuthErrors.swift
//  iChat
//
//  Created by Андрей Журавлев on 07.05.2020.
//  Copyright © 2020 Андрей Журавлев. All rights reserved.
//

import Foundation

enum AuthError {
    case notFilled, invalidEmail, invalidPassword, notMatchingPasswords, serverError, unknownError
}

extension AuthError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .notFilled:
            return NSLocalizedString("Заполните все поля", comment: "")
        case .invalidEmail:
            return NSLocalizedString("Неправильный email", comment: "Проверьте правильность вашего адреса электронной посты")
        case .invalidPassword:
            return NSLocalizedString("Неправильный пароль", comment: "Убедитесь, что пароль состоит минимум из 8 символов, содержит специальный символ и заглваную букву")
        case .notMatchingPasswords:
            return NSLocalizedString("Пароли не совпадают", comment: "Проверьте правильность паролей")
        case .serverError:
            return NSLocalizedString("Ошибка сервера", comment: "")
        case .unknownError:
            return NSLocalizedString("Неисзвестная ошибка", comment: "Проверьте правильность заполнения всех полей")
        }
    }
}
