//
//  StorageErrors.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 09.12.2020.
//

import Foundation

enum StorageError {
    case dataCompression, uploading, downloading
}

extension StorageError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .dataCompression:
            return "Ошибка сжатия данных"
        case .uploading:
            return "Ошибка сохранения данных на сервере"
        case .downloading:
            return "Ошибка загрузки данных с сервера"
        }
    }
}
