//
//  UserErrors.swift
//  iChat
//
//  Created by Андрей Журавлев on 08.05.2020.
//  Copyright © 2020 Андрей Журавлев. All rights reserved.
//

import Foundation

enum UserError {
	case notFilled, canNotGetUserInfo, unableToDecode
}

extension UserError: LocalizedError {
	var errorDescription: String? {
		switch self {
			case .notFilled:
				return NSLocalizedString("Заполните все поля", comment: "")
			case .canNotGetUserInfo:
				return NSLocalizedString("Ошибка загрузки данных пользователя с сервера", comment: "")
			case .unableToDecode:
				return NSLocalizedString("Ошибка парсинга данных", comment: "")
		}
	}
}
