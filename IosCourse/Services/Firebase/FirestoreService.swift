//
//  FirestoreService.swift
//  iChat
//
//  Created by Андрей Журавлев on 08.05.2020.
//  Copyright © 2020 Андрей Журавлев. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore


final class FirestoreService {
    
    static let shared = FirestoreService()
    private init() {}
    
    let db = Firestore.firestore()
    private var usersRef: CollectionReference {
        return db.collection(FirebaseCodingKeys.users)
    }
    private var pilotsRef: CollectionReference {
        return db.collection(FirebaseCodingKeys.pilots)
    }
    private var placesRef: CollectionReference {
        return db.collection(FirebaseCodingKeys.places)
    }
    
    func getUserData(uid: String, completion: @escaping (Result<CurrentUserModel, Error>) -> ()) {
        let docRef = usersRef.document(uid)
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                guard var user = CurrentUserModel(document: document) else {
                    completion(.failure(UserError.unableToDecode))
                    return
                }
                
                if user.isPilot {
                    self.getPilotData(uid: user.uid) { (result) in
                        switch result {
                        case .success(let pilotModel):
                            user.pilotModel = pilotModel
                            completion(.success(user))
                        case .failure(_):
                            completion(.failure(UserError.canNotGetUserInfo))
                        }
                    }
                }
                
                completion(.success(user))
            } else {
                completion(.failure(UserError.canNotGetUserInfo))
            }
        }
    }
    
    
    func saveProfile(withUid uid: String, firstName: String?, lastName: String?, email: String,completion: @escaping (Result<CurrentUserModel, Error>) -> ()) {
        
        guard Validators.isFilled(firstName: firstName, lastName: lastName) else {
            completion(.failure(UserError.notFilled))
            return
        }
        
        let user = CurrentUserModel(uid: uid, firstName: firstName!, lastName: lastName!, email: email)
        
        usersRef.document(user.uid).setData(user.dictionaryRepresentation) { (error) in
            if let error = error {
                completion(.failure(error))
            }
            completion(.success(user))
        }
    }
    
    func becomePilot(pilotModel: PilotModel,completion: @escaping (Result<Any?, Error>) -> ()) {
        usersRef.document(pilotModel.uid).setData(["isPilot": true], merge: true)
        
        pilotsRef.document(pilotModel.uid).setData(pilotModel.dictionaryRepresentation) { (error) in
            if let error = error {
                completion(.failure(error))
            }
            completion(.success(nil))
        }
    }
    
    func addWork(pilotUid: String, workId:String, completion: @escaping (Result<Any?, Error>) -> ()) {
        pilotsRef.document(pilotUid).getDocument(completion: { [weak self] (document, error) in
            guard error != nil, let document = document else {
                return
            }
            
            guard var worksIds = document.data()?["worksIds"] as? [String] else {
                return
            }
            
            worksIds.append(workId)
            self?.pilotsRef.document(pilotUid).setData(["worksIds": worksIds], merge: true)
        })
       
    }
    
    func savePlace(withUid id: String, name: String?, descriptionPlace: String?, latitude: Double, longitude: Double, imagePaths: [String], completion: @escaping (Result<PlaceModel, Error>) -> ()) {
        
        guard let userUid = CurrentUserManager.shared.currentUser?.value?.uid else {
            completion(.failure(StorageError.uploading))
            return
        }
        
        let place = PlaceModel(id: id,
                               ownerId: userUid,
                               name: name!,
                               descriptionPlace: descriptionPlace!,
                               latitude: latitude,
                               longitude: longitude,
                               imagePaths: imagePaths)
        
        placesRef.document(place.id).setData(place.dictionaryRepresentation) { (error) in
            if let error = error {
                completion(.failure(error))
            }
            completion(.success(place))
        }
    }
    
    func getPlaces(completion: @escaping (Result<[PlaceModel], Error>) -> ()) {
        placesRef.addSnapshotListener { querySnapshot, error in
            guard let documents = querySnapshot?.documents else {
                completion(.failure(StorageError.downloading))
                return
            }
            let placeModels = documents.map {
                //TODO: FIX !
                PlaceModel(document: $0)!
            }
            
            completion(.success(placeModels))
        }
    }
    
    func getWorks(ownerId: String, completion: @escaping (Result<[WorkModel], Error>) -> ()) {
        
        placesRef.whereField("ownerId", isEqualTo: ownerId).getDocuments { (documents, error) in
            if let documents = documents {
                
                let dispatchGroup = DispatchGroup()
                
                var workModels: [WorkModel] = []
                
                for document in documents.documents {
                    dispatchGroup.enter()
                    
                    guard let placeModel = PlaceModel(document: document) else {
                        completion(.failure(UserError.unableToDecode))
                        return
                    }
                    
                    self.getPilotData(uid: placeModel.ownerId) { (result) in
                        switch result {
                        case .success(let pilotModel):
                            if let imagePath = placeModel.imagePaths.first {
                                StorageService.shared.getImage(photoPath: imagePath) { (result) in
                                    switch result {
                                    case .success(let image):
                                        dispatchGroup.leave()
                                        let workModel = WorkModel(placeModel: placeModel,
                                                                  pilotModel: pilotModel, image: image)
                                        workModels.append(workModel)
                                    case .failure(let error):
                                        completion(.failure(error))
                                    }
                                }
                            }else {
                                
                            }
                        case .failure(let error):
                            completion(.failure(error))
                        }
                    }
                    
                }
                
                dispatchGroup.notify(queue: .main) {
                    completion(.success(workModels))
                }
            }
        }
    }
    
    func savePilotData(withUid uid: String, firstName: String?, lastName: String?, email: String,completion: @escaping (Result<CurrentUserModel, Error>) -> ()) {
        
        guard Validators.isFilled(firstName: firstName, lastName: lastName) else {
            completion(.failure(UserError.notFilled))
            return
        }
        
        let user = CurrentUserModel(uid: uid, firstName: firstName!, lastName: lastName!, email: email)
        
        usersRef.document(user.uid).setData(user.dictionaryRepresentation) { (error) in
            if let error = error {
                completion(.failure(error))
            }
            completion(.success(user))
        }
    }
    
    func getPilotData(uid: String, completion: @escaping (Result<PilotModel, Error>) -> ()) {
        let docRef = pilotsRef.document(uid)
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                
                guard var pilot = PilotModel(document: document) else {
                    completion(.failure(UserError.unableToDecode))
                    return
                }
                
                StorageService.shared.getImage(photoPath: pilot.profileImagePath) { (result) in
                    switch result {
                    case .success(let image):
                        pilot.profileImage = image
                        completion(.success(pilot))
                    case .failure(_):
                        completion(.failure(UserError.unableToDecode))
                    }
                }
            } else {
                completion(.failure(UserError.canNotGetUserInfo))
            }
        }
    }
    
    func getPilotsData(completion: @escaping (Result<[PilotModel], Error>) -> ()) {
        let docsRef = pilotsRef
        docsRef.addSnapshotListener { (snapshot, error) in
            if let documents = snapshot?.documents {
                
                let dispatchGroup = DispatchGroup()
                
                var pilots: [PilotModel] = []
                
                for document in documents {
                    dispatchGroup.enter()
                    guard var pilot = PilotModel(document: document) else {
                        completion(.failure(UserError.unableToDecode))
                        return
                    }
                    
                    StorageService.shared.getImage(photoPath: pilot.profileImagePath) { (result) in
                        switch result {
                        case .success(let image):
                            dispatchGroup.leave()
                            pilot.profileImage = image
                            pilots.append(pilot)
                        case .failure(_):
                            completion(.failure(UserError.unableToDecode))
                        }
                    }
                }
                
                dispatchGroup.notify(queue: .main) {
                    completion(.success(pilots))
                }
            }
        }
    }
    
}
