//
//  FirebaseCodingkKeys.swift
//  iChat
//
//  Created by Андрей Журавлев on 13.05.2020.
//  Copyright © 2020 Андрей Журавлев. All rights reserved.
//

import Foundation

struct FirebaseCodingKeys {
	static let users = "Users"
    static let pilots = "Pilots"
    static let places = "Places"
}

