//
//  StorageService.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 09.12.2020.
//

import Foundation
import FirebaseStorage

final class StorageService {
    
    static let shared = StorageService()
    private init() {}
    
    let storage = Storage.storage()
//    private var usersRef: CollectionReference {
//        return db.collection(FirebaseCodingkKeys.users)
//    }
//    private var placesRef: CollectionReference {
//        return db.collection(FirebaseCodingkKeys.places)
//    }
    
    func savePlacePhoto(placeId: String, images: [UIImage], completion: @escaping (Result<[String], Error>) -> ()) {
        
        let dispatchGroup = DispatchGroup()
        var urls: [String] = []

        for image in images {
            dispatchGroup.enter()
            guard let imageData = image.jpegData(compressionQuality: 0.25) else {
                completion(.failure(StorageError.dataCompression))
                return
            }
            
            let metaData = StorageMetadata()
            metaData.contentType = "image/jpg"
            let imageId = UUID().uuidString
            let imageRef = storage.reference().child("\(placeId)/\(imageId).jpg")
            
            imageRef.putData(imageData, metadata: metaData) { ( _, error) in
                
                dispatchGroup.leave()
                
                guard error == nil else {
                    completion(.failure(StorageError.uploading))
                    return
                }
                
                urls.append(imageRef.fullPath)
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            completion(.success(urls))
        }
    }
    
    func getPlacePhotos(photoPaths: [String], completion: @escaping (Result<[UIImage], Error>) -> ()) {
        
        let dispatchGroup = DispatchGroup()
        var photoImages: [UIImage] = []
        
        for path in photoPaths {
            dispatchGroup.enter()
            
            let imageRef = storage.reference().child(path)
            imageRef.getData(maxSize: 1 * 1048 * 1048) { (data, error) in
                dispatchGroup.leave()
                
                guard error == nil, let data = data, let image = UIImage(data: data) else {
                    completion(.failure(StorageError.downloading))
                    return
                }
                
                photoImages.append(image)
                
            }
            
        }
        
        dispatchGroup.notify(queue: .main) {
            completion(.success(photoImages))
        }
    }
    
    
    func saveProfileImage(uid: String, image: UIImage, completion: @escaping (Result<String, Error>) -> ()) {
        
        guard let imageData = image.jpegData(compressionQuality: 0.25) else {
            completion(.failure(StorageError.dataCompression))
            return
        }
        
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        let imageId = UUID().uuidString
        let imageRef = storage.reference().child("\(uid)/\(imageId).jpg")
        
        imageRef.putData(imageData, metadata: metaData) { ( _, error) in
            guard error == nil else {
                completion(.failure(StorageError.uploading))
                return
            }
            completion(.success(imageRef.fullPath))
        }
        
    }
    
    func getImage(photoPath: String, completion: @escaping (Result<UIImage, Error>) -> ()) {
        
        let imageRef = storage.reference().child(photoPath)
        imageRef.getData(maxSize: 1 * 1048 * 1048) { (data, error) in
            
            guard error == nil, let data = data, let image = UIImage(data: data) else {
                completion(.failure(StorageError.downloading))
                return
            }
            
            completion(.success(image))
        }
    }
    
    
}
