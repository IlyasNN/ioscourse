//
//  LocationService.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 30.11.2020.
//

import Foundation
import CoreLocation

protocol LocationServiceable: class {
    
    var isEnabled: Bool { get }
    
    var permissionStatus: Bool { get }

    func startUpdatingLocation() -> LocationServiceError?
    func stopUpdatingLocation()

    var currentLocation: Emitter<LocationService.Location?> { get }
}

enum LocationServiceError: Error {
    case notEnabled
    case needAskUserPermission
    case userDidNotAllowed
}

class LocationService: NSObject {
    
    struct Location: Equatable {
        let latitude: Double
        let longitude: Double
    }
    
    let locationManager = CLLocationManager()
    var currentLocation = Emitter<LocationService.Location?>(nil)
    
    static let shared = LocationService()
    private override init() {
        super.init()
        
        self.locationManager.activityType = .automotiveNavigation
      //  self.locationManager.allowsBackgroundLocationUpdates = true
    //    self.locationManager.pausesLocationUpdatesAutomatically = false
        self.locationManager.delegate = self
    }
}

extension LocationService: LocationServiceable {
    
    func startUpdatingLocation() -> LocationServiceError? {
        
        if !isEnabled {
            print("ERROR: location service is not enabled")
            return .notEnabled
        }
        
        switch locationManager.authorizationStatus {
        
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
            return .needAskUserPermission
        case .restricted, .denied:
            return .userDidNotAllowed
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            return nil
        @unknown default:
            return nil
        }

    }
    
    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
    
    var isEnabled: Bool {
        CLLocationManager.locationServicesEnabled()
    }
    
    var permissionStatus: Bool {
        
        switch locationManager.authorizationStatus {
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
            return true
        case .restricted, .denied:
            return false
        case .authorizedAlways, .authorizedWhenInUse:
            if #available(iOS 14.0, *) {
                let accuracyAuthorization = locationManager.accuracyAuthorization
                switch accuracyAuthorization {
                case .fullAccuracy:
                    return true
                case .reducedAccuracy:
                    return false
                @unknown default:
                    return false
                }
            }
            return true
        @unknown default:
            return false
        }
    }

}

extension LocationService: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // .requestLocation will only pass one location to the locations array
        if let clLocation = locations.first {
            print("Location: \(locations.first?.description ?? "empty")")
            
            let location = Location(latitude: clLocation.coordinate.latitude,
                                    longitude: clLocation.coordinate.longitude)
            currentLocation.value = location
        }
    }
    
    
    // This delegate method (didChangeAuthorization status) will always be called after the line locationManager.delegate = self, even when the permission dialog haven't been shown to user before.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("location manager authorization status changed")
        
        switch status {
        case .authorizedAlways:
            _ = startUpdatingLocation()
        case .authorizedWhenInUse:
            _ = startUpdatingLocation()
        case .denied, .restricted, .notDetermined:
            return
        @unknown default:
            print("ERROR: Unhendeled CLLocationManager Authorization status")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("ERROR: locationManager didFailWithError: \(error.localizedDescription)")
        // might be that user didn't enable location service on the device
        // or there might be no GPS signal inside a building
      
        // might be a good idea to show an alert to user to ask them to walk to a place with GPS signal
    }
}




