//
//  ImagePickerService.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 09.12.2020.
//

import UIKit
import Photos

protocol ImagePickerServiceDelegate: class {
    func imagePickerService(finishWith image: UIImage)
    func imagePickerService(finishWith error: ImagePickerError)
}

enum ImagePickerError {
    case noRightsForLibrary
    case noRightsForCamera
}

class ImagePickerService: NSObject {

    weak var presentingViewController: UIViewController?
    weak var delegate: ImagePickerServiceDelegate?
    
    public func pickPhotoFromLibrary(allowsEditing: Bool = false) {
        accessLibrary(allowsEditing: allowsEditing)
    }
    
    public func pickPhotoFromCamera(allowsEditing: Bool = false) {
        accessCamera(allowsEditing: allowsEditing)
    }


    private func accessLibrary(allowsEditing: Bool) {
        guard UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) else {
            return
        }
        
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized:
            showLibrary(allowsEditing: allowsEditing)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization { [weak self] status in
                if status == .authorized {
                    self?.showLibrary(allowsEditing: allowsEditing)
                }
            }
        case .denied, .restricted, .limited:
            delegate?.imagePickerService(finishWith: .noRightsForLibrary)
            
        @unknown default:
            print("ERROR: Unhandeled PHPhotoLibrary status")
        }
    }
    
    private func showLibrary(allowsEditing: Bool) {
        DispatchQueue.main.async {
            let picker = UIImagePickerController()
            picker.delegate = self

            picker.allowsEditing = allowsEditing
            picker.sourceType = .savedPhotosAlbum

            self.presentingViewController?.present(picker, animated: true, completion: nil)
        }
    }
    
    private func accessCamera(allowsEditing: Bool) {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            print("No camera");
            return
        }
        
        
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        switch status {
        case .authorized:
            self.openCamera(allowsEditing: allowsEditing)
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { [weak self] granted in
                if granted {
                    self?.openCamera(allowsEditing: allowsEditing)
                }
            }
        case .denied, .restricted:
            delegate?.imagePickerService(finishWith: .noRightsForCamera)
        default:
            break
        }
    }
    
    private func openCamera(allowsEditing: Bool) {
        DispatchQueue.main.async {
            let picker = UIImagePickerController()
            picker.delegate = self

            picker.allowsEditing = allowsEditing
            picker.mediaTypes = ["public.image"]
            picker.sourceType = .camera
            picker.cameraCaptureMode = .photo
            picker.cameraDevice = .rear

            self.presentingViewController?.present(picker, animated: true, completion: nil)
        }
    }
    
}

// MARK: - UIImagePickerControllerDelegate

extension ImagePickerService: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) {
            if (picker.sourceType == .savedPhotosAlbum) || (picker.sourceType == .camera) {
                if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage ?? info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                    self.delegate?.imagePickerService(finishWith: image)
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - UINavigationControllerDelegate

extension ImagePickerService: UINavigationControllerDelegate {
    
}

