//
//  CoreDataService.swift
//  IosCourse
//
//  Created by Илья Соловьёв on 11.11.2020.
//

import Foundation
import CoreData

//MARK: EntitiesEnum

enum Entities {
    case CurrentUserEntity(_ model: CurrentUserModel? = nil)
    case PilotEntity(_ model: PilotModel? = nil)
}


//MARK: ServiceProtocol

protocol CoreDataServiceable {
    
    static var shared: CoreDataService { get }
    
    func addEntity(entity: Entities)

}

//MARK: General

final class CoreDataService: NSObject, CoreDataServiceable {
    
    
    // MARK: Singleton
    static let shared = CoreDataService()
    private override init() {
        super.init()
        
    }
    
    
    // MARK: Methods
    
    func entityForName(entityName: String) -> NSEntityDescription {
        return NSEntityDescription.entity(forEntityName: entityName, in: CoreDataService.backgroundContext)!
    }
    
    func getCurrentUser() -> CurrentUserEntity? {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "CurrentUserEntity")
        let context = CoreDataService.backgroundContext
        
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            let objects = try context.fetch(fetchRequest)
            if objects.isEmpty {
                return nil
            }
            return objects.first as? CurrentUserEntity
        } catch {
            let fetchError = error as NSError
            print("Unable to Delete Note")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
        return nil
    }
    
    func getCurrentPilot() -> PilotEntity? {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "PilotEntity")
        let context = CoreDataService.backgroundContext
        
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            let objects = try context.fetch(fetchRequest)
            if objects.isEmpty {
                return nil
            }
            return objects.first as? PilotEntity
        } catch {
            let fetchError = error as NSError
            print("Unable to Delete Note")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
        return nil
    }
    
    func changeCurrentUserEntity(currentUserModel: CurrentUserModel) {
        let currentUser = getCurrentUser()
        currentUser?.isPilot = currentUserModel.isPilot
        saveChanges()
    }
    
    
    func addEntity(entity: Entities) {
        switch entity {
        case .CurrentUserEntity(let model):
            guard let model = model else {
                return
            }
            _ = CurrentUserEntity(model: model)
        case .PilotEntity(let model):
            guard let model = model else {
                return
            }
            _ = PilotEntity(model: model)
        }
        
        saveBackgroundContext()
    }
    
    func saveChanges() {
        saveBackgroundContext()
    }
    
    
    public func clearAllCoreData() {
        let entities = CoreDataService.persistentContainer.managedObjectModel.entities
        entities.compactMap({ $0.name }).forEach(clearDeepObjectEntity)
    }
    
    private func clearDeepObjectEntity(_ entity: String) {
        let context = CoreDataService.backgroundContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print(error, error.localizedDescription)
        }
    }
    
    private func saveBackgroundContext() {
        let context = CoreDataService.backgroundContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
    
    // MARK: - FetchedResultsControllers
    
   
    
    // MARK: - Core Data stack
    
    static var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "IosCourseDataModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    static var backgroundContext = CoreDataService.persistentContainer.newBackgroundContext()
    
}
