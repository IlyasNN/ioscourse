//
//  CurrentUserEntity+CoreDataProperties.swift
//  
//
//  Created by Илья Соловьёв on 11.11.2020.
//
//

import Foundation
import CoreData


extension CurrentUserEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CurrentUserEntity> {
        return NSFetchRequest<CurrentUserEntity>(entityName: "CurrentUserEntity")
    }

    @NSManaged public var firstName: String
    @NSManaged public var lastName: String
    @NSManaged public var uid: String
    @NSManaged public var email: String
    @NSManaged public var isPilot: Bool
    
    convenience init(model: CurrentUserModel) {
        self.init(entity: CoreDataService.shared.entityForName(entityName: "CurrentUserEntity"), insertInto: CoreDataService.backgroundContext)
        
        self.uid = model.uid
        self.firstName = model.firstName
        self.lastName = model.lastName
        self.email = model.email
        self.isPilot = model.isPilot
    }

}
