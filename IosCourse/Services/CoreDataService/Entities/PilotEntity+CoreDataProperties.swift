//
//  PilotEntity+CoreDataProperties.swift
//  
//
//  Created by Илья Соловьёв on 13.12.2020.
//
//

import Foundation
import CoreData


extension PilotEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PilotEntity> {
        return NSFetchRequest<PilotEntity>(entityName: "PilotEntity")
    }

    @NSManaged public var phoneNumber: String
    @NSManaged public var uid: String
    @NSManaged public var nameSurname: String
    @NSManaged public var copterName: String
    @NSManaged public var profileImagePath: String
    @NSManaged public var profileImage: Data?
    @NSManaged public var worksIds: [String]

    convenience init(model: PilotModel) {
        self.init(entity: CoreDataService.shared.entityForName(entityName: "PilotEntity"), insertInto: CoreDataService.backgroundContext)
        
        self.uid = model.uid
        self.nameSurname = model.nameSurname
        self.phoneNumber = model.phoneNumber
        self.copterName = model.copterName
        self.profileImagePath = model.profileImagePath
        self.profileImage = model.profileImage?.jpegData(compressionQuality: 1)
        self.worksIds = model.worksIds
    }
}
